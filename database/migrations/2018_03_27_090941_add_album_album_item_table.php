<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlbumAlbumItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_album_item', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('album_id')->unsigned();
            $table->integer('album_item_id')->unsigned();

            // $table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade');
            // $table->foreign('album_item_id')->references('id')->on('album_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_album_item');
    }
}
