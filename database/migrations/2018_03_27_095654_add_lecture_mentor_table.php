<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLectureMentorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecture_mentor', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lecture_id')->unsigned();
            $table->integer('mentor_id')->unsigned();

            // $table->foreign('lecture_id')->references('id')->on('lectures')->onDelete('cascade');
            // $table->foreign('mentor_id')->references('id')->on('mentors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecture_mentor');
    }
}
