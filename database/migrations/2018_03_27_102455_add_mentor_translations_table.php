<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMentorTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mentor_translations', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('mentor_id')->unsigned();
            $table->string('locale')->index();

            $table->string('name');
            $table->string('profession');
            $table->text('description');

            $table->string('skill_one')->nullable();
            $table->string('skill_two')->nullable();
            $table->string('skill_three')->nullable();

            $table->unique(['mentor_id','locale']);
            $table->foreign('mentor_id')->references('id')->on('mentors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mentor_translations');
    }
}
