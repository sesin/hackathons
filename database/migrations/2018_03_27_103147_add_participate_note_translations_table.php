<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParticipateNoteTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participate_note_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('participate_note_id')->unsigned();
            $table->string('locale')->index();
            
            $table->string('title');
            $table->text('content');

            $table->unique(['participate_note_id', 'locale']);
            $table->foreign('participate_note_id')->references('id')->on('participate_notes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participate_note_translations');
    }
}
