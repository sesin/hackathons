<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'id' => 1,
            'contacts' => '{"button_url":null}',
        ]);

        DB::table('contacts_translations')->insert([
            'id' => 1,
            'locale' => 'en',
            'translated_contacts' => '{"main_text":null,"button_text":null}',
            'contacts_id' => 1,
        ]);

        DB::table('contacts_translations')->insert([
            'id' => 2,
            'locale' => 'ru',
            'translated_contacts' => '{"main_text":null,"button_text":null}',
            'contacts_id' => 1,
        ]);
    }
}
