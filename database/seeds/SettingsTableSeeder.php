<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'id' => 1,
            'settings' => '{"banner_image_path":null,"button_url":null,"emails":null}',
        ]);

        DB::table('settings_translations')->insert([
            'id' => 1,
            'locale' => 'en',
            'translated_settings' => '{"banner_title":null,"banner_text":null,"about_hackathon_text":null,"about_hackathon_announcement":null,"button_text":null,"rules_file_path":null}',
            'settings_id' => 1,
        ]);

        DB::table('settings_translations')->insert([
            'id' => 2,
            'locale' => 'ru',
            'translated_settings' => '{"banner_title":null,"banner_text":null,"about_hackathon_announcement":null,"about_hackathon_text":null,"button_text":null,"rules_file_path":null}',
            'settings_id' => 1,
        ]);
    }
}
