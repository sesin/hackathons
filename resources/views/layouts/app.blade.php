<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('main.meta-title') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <header id="header">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="/">
                        <img src="/images/logo.png" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-top" aria-controls="navbar-top" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar-top">
                        <ul class="navbar-nav" id="main-menu">
                            <li class="nav-item">
                                <a class="nav-link scroll-link" href="#about-project">{{ trans('main.about-the-hackathon') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll-link" href="#why-section">{{ trans('main.why-to-participate') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll-link" href="#mentors-section">{{ trans('main.mentors') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll-link" href="#agenda-section">{{ trans('main.agenda') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll-link" href="#was-section">{{ trans('main.how-it-was') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll-link" href="#rules-section">{{ trans('main.rules-and-faq') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll-link" href="#partners-section">{{ trans('main.partners') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link external" href="/register-form">{{ trans('main.registration') }}</a>
                            </li>
                        </ul>
                        <ul class="navbar-nav" id="lang-menu">
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('*en*') ? 'active' : '' }}" href="/lang/en">EN</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('*ru*') ? 'active' : '' }}" href="/lang/ru">Рус</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>

        <main>
            @if(Session::has('flash_message'))
                <div class="container">      
                    <div class="alert alert-success">
                        <em>{!! session('flash_message') !!}</em>
                    </div>
                </div>
            @endif

            @yield('content')
        </main>
    </div>
    <footer id="footer">
        <div class="container">
            <div class="inner">
                <div class="footer-line footer-line-1">
                    <a href="#" target="_blank" class="footer-logo">
                        <img src="/images/footer_logo_1.png" alt="">
                    </a>
                    <div class="copy">
                        {!! trans('main.footer-eu') !!}
                    </div>
                </div>
                <div class="footer-line footer-line-2">
                    <a href="#" target="_blank" class="footer-logo">
                        <img src="/images/footer_logo_2.jpg" alt="">
                    </a>
                    <div class="copy">
                        {!! trans('main.footer-gdsi') !!}
                    </div>
                </div>
                <div class="footer-line footer-line-3">
                    <div class="soc-block">
                        <a href="#" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" target="_blank" class="tw"><i class="fa fa-twitter"></i></a>
                        <a href="#" target="_blank" class="in"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div id="scroll-up">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5abb892f003b52001341afad&product=custom-share-buttons"></script>
</body>
</html>
