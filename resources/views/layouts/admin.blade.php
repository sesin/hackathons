<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="/favicon.ico">
    <link href="/css/admin.css" rel="stylesheet">

    <title>@section('title')Админ. панель@show</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="{{ route('dashboard') }}">Админ. панель</a>
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}">На сайт</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard') }}">Административная панель</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('news.index') }}">Новости</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('participate-notes.index') }}">Секция "Что ты получишь"</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('partners.index') }}">Партнеры</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('mentors.index') }}">Менторы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('days.index') }}">Дни</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('lectures.index') }}">Лекции</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('rules.index') }}">Правила и FAQ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('contacts.index') }}">Контакты</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('albums.index') }}">Как это было в 2017</a>
                    </li>

                </ul>
            </nav>
            <main role="main" class="ml-sm-auto col-md-10 pt-3">

                @if(Session::has('flash_message'))
                    <div class="container">      
                        <div class="alert alert-success">
                            <em>{!! session('flash_message') !!}</em>
                        </div>
                    </div>
                @endif

                @if(Session::has('error_message'))
                    <div class="container">      
                        <div class="alert alert-danger">
                            <em>{!! session('error_message') !!}</em>
                        </div>
                    </div>
                @endif

                <h1>@section('header')Административная панель@show</h1>

                <div>
                    @yield('content')
                </div>
            </main>
        </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/app.js"></script>
    <script src="/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
                selector: ".tinymce-val",
                language: "ru",
                theme: "modern",
                width: "100%",
                height: 300,
                convert_urls: false,
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                     "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                     "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
               ],
               toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
               toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
               image_advtab: true,
               
               external_filemanager_path: "/filemanager/",
               filemanager_title: "Responsive Filemanager",
               filemanager_access_key: "EGDT3dyjsMFxYV865EasV6e5RHkRXDCas8r7t2mv",
               external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
            });
    </script>
</body>
</html>