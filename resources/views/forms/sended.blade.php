@extends('layouts.app')

@section('content')
	<style>
		body {
	        display: -webkit-box;
	        display: -ms-flexbox;
	        display: flex;
	    }
	    #app {
	        position: relative;
	    }
	</style>
	
    <div class="container">
        <h1 class="text-center form-title">EaP Civil Society Hackathon</h1>
        <h2 class="text-center form-subtitle">CONGRATULATIONS!</h2>
        <p class="text-center">Your application form was successfully submitted.</p>
        <p class="text-center">To return to the main page, click on the button.</p>
        <div class="text-center"><a class="btn btn-primary" href="{{route('home')}}">HOME</a></div>
    </div>
@endsection
