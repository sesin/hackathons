<style type="text/css">
    .counter-wrap{
        margin-top: 30px;
    }
</style>
    <div class="container">
        <h1 class="text-center form-title">EaP Civil Society Hackathon</h1>
        <h4 class="text-center">Регистрация на Хакатон гражданского общества стран Восточного партнерства – 2018 начнется 9 апреля 2018 /</h4>
        <h4 class="text-center">Registration for 2018 EaP Civil Society Hackathon will start on 9 April 2018.</h4>
 
            <div class="col-xl-4 offset-xl-4 counter-block">
                <div class="counter-wrap">
                    <div class="block-title" data-end-text="Hackathon has already started:">THE REGISTRATION STARTS IN</div>
                    <div class="start-counter" data-countdown="2018/4/9"></div>
                </div>
            </div>
    </div>
