@extends('layouts.app')

@section('content')
    
    <style>
        body {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }
        #app {
            position: relative;
        }
    </style>

    <div class="container">
        <h1 class="text-center form-title">{{ trans('form.eap-civil-society-hackathon') }}</h1>
        <h2 class="text-center form-subtitle">{{ trans('form.register') }}</h2>
        <div class="col-md-4 offset-md-4">
            <div class="progress hexa-progress">
                <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>            
        </div>
    </div>
    <div id="registration-forms" class="container">
        <h3 class="text-center form-subtitle2">{{ trans('form.select-registration-form') }}</h3>

            <div class="col-md-8 offset-md-2">
                <a class="pointer hexa-btn" data-form="activists">{{ trans('form.application-for-activists') }}</a>
            </div>

            <div class="col-md-8 offset-md-2">
                <a class="pointer hexa-btn" data-form="professionals">{{ trans('form.application-for-professionals') }}</a>
            </div>

    </div>

    @include('forms.activists')

    @include('forms.professionals')
    <div class="container">
        <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1 text-right pb-4">
            <input type="button" id="prev" class="btn btn-primary" value="Prev" style="display: none">
            <input type="button" id="next" class="btn btn-primary" value="Next" style="display: none;">
            <input type="button" id="submit" class="btn btn-primary" value="Submit" style="display: none">
        </div>
    </div>

@endsection
