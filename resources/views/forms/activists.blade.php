<div id="activists" class="container form-hidden">
<div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
    {{ Form::open(['id' => 'activists-form', 'route' => 'activists.form.store', 'method' => 'post', 'files' => true]) }}
        <div class="tab">
            {{ Form::hidden('type', 'activist') }}
            
            <div class="form-group">
                <div><label for="">{{ trans('form.title') }}:</label></div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Title]', 'Mr', false,
                        ['id' => 'mr-activists', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('mr-activists', trans('form.mr'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Title]', 'Mrs', false,
                        ['id' => 'mrs-activists', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('mrs-activists', trans('form.mrs'), ['class' => 'form-check-label']) }}
                </div>
                
                @if (App::isLocale('en'))

                    <div class="form-check form-check-inline">
                        {{ Form::radio('formData[Title]', 'Ms', false,
                            ['id' => 'ms-activists', 'class' => 'form-check-input']
                        )}}
                        {{ Form::label('ms-activists', 'Ms', ['class' => 'form-check-label']) }}
                    </div>

                @endif
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <h3>1.1 {{ trans('form.first-name') }}:</h3>    
                    {{ Form::text('formData[firstName]', null, ['id' => 'first-name-activists', 'class' => 'form-control'])}}
                </div>
                <div class="form-group col-md-6">
                    <h3>1.2 {{ trans('form.last-name') }}:</h3>
                    {{ Form::text('formData[lastName]', null, ['id' => 'last-name-activists', 'class' => 'form-control'])}}
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <h3>1.3 {{ trans('form.contact-phone') }}:</h3>
                    {{ Form::text('formData[Phone]', null, ['id' => 'phone-activists', 'class' => 'form-control'])}}
                </div>
                <div class="form-group col-md-6">
                    <h3>1.4 {{ trans('form.email') }}</h3>
                    {{ Form::email('formData[Email]', null, ['id' => 'email-activists', 'class' => 'form-control'])}}
                </div>
            </div>

            <div class="form-group">
                <h3>1.5 {{ trans('form.residential-address') }}:</h3>
            </div>

            <div class="form-group">
                {{ Form::label('steet-house-apartment', trans('form.street-house-apartment')) }}
                {{ Form::text('formData[streetHouseApartment]', null, ['id' => 'steet-house-apartment-activists', 'class' => 'form-control'])}}
            </div>

            <div class="form-group">
                {{ Form::label('city', trans('form.city-town')) }}
                {{ Form::text('formData[City]', null, ['id' => 'city-activists', 'class' => 'form-control'])}}
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    {{ Form::label('residential-country', trans('form.country')) }}
                    {{ Form::select('formData[residentialCountry]', $countries, null, 
                        [
                            'id' => 'residential-country-activists',
                            'class' => 'form-control hexa-select',
                            'placeholder' => trans('form.please-choose')
                        ]
                    )}}
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <h3>1.6 {{ trans('form.citizenship') }}</h3>
                    {{ Form::select('formData[citizenshipCountry]', $countries, null,
                        [
                            'id' => 'citizenship-country-activists',
                            'class' => 'form-control',
                            'placeholder' => trans('form.please-choose')
                        ]
                    )}}
                </div>
            </div>

            <div class="form-group">
                <h3>1.7 {{ trans('form.gender') }}:</h3>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Gender]', 'Male', false,
                        ['id' => 'male-activists', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('male-activists', trans('form.male'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Gender]', 'Female', false,
                        ['id' => 'female-activists', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('female-activists', trans('form.female'), ['class' => 'form-check-label']) }}
                </div>
            </div>
            
            <div class="form-group">
                <h3>1.8 {{ trans('form.language-skills') }}:</h3>
            </div>

            <div class="form-row form-group">
                <div class="col-md-2 align-self-center">
                    <label class="align-self-center">{{ trans('form.russian') }}:</label>
                </div>
                <div class="col-md-10 form-row">
                    <div class="col-md-4 align-self-end">{{ Form::label('reading-ru-activists', trans('form.reading')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('speaking-ru-activists', trans('form.speaking')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('listening-ru-activists', trans('form.listening-comprehension')) }}</div>
                    <div class="col-md-4">
                        {{ Form::select('formData[readingRu]', $language_levels, null, 
                            [
                                'id' => 'reading-ru-activists',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[speakingRu]', $language_levels, null,
                            [
                                'id' => 'speaking-ru-activists',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[listeningRu]', $language_levels, null, 
                            [
                                'id' => 'listening-ru-activists',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                </div>
            </div>

            <div class="form-row form-group">
                <div class="col-md-2 align-self-center">
                    <label class="align-self-center">{{ trans('form.english') }}:</label>
                </div>
                <div class="col-md-10 form-row">
                    <div class="col-md-4 align-self-end">{{ Form::label('reading-en-activists', trans('form.reading')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('speaking-en-activists', trans('form.speaking')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('listening-en-activists', trans('form.listening-comprehension')) }}</div>
                    <div class="col-md-4">
                        {{ Form::select('formData[readingEn]', $language_levels, null, 
                            [
                                'id' => 'reading-en-activists',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[speakingEn]', $language_levels, null,
                            [
                                'id' => 'speaking-en-activists',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[listeningEn]', $language_levels, null, 
                            [
                                'id' => 'listening-en-activists',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                </div>
            </div>
        </div>
        
        <div class="tab">
            <div class="form-group">
                <h3>2.1 {{ trans('form.name-of-ict-tool') }}:</h3>
                {{ Form::textarea('formData[ictToolName]', null,
                    ['id' => 'ict-tool-name', 'class' => 'form-control', 'maxlength' => '1500']
                )}}
            </div>

            <div class="form-group">
                <h3>2.2 {{ trans('form.target-of-ict-tool') }}:</h3>
                {{ Form::textarea('formData[targetTowns]', null,
                    ['id' => 'target-towns', 'class' => 'form-control', 'maxlength' => '1500']
                )}}
            </div>

            <div class="form-group">
                <h3>2.3 {{ trans('form.describe-ict-tool') }}:</h3>
                {{ Form::textarea('formData[toolDescription]', null,
                    ['id' => 'tool-desciption', 'class' => 'form-control', 'maxlength' => '1500']
                )}}
            </div>
            

            <div class="form-group">
                <h3>2.4 {{ trans('form.problem-to-solve') }}:</h3>
                {{ Form::textarea('formData[Problem]', null,
                    [
                        'id' => 'problem', 'class' => 'form-control',
                        'placeholder' => trans('form.describe-social-problem'),
                        'maxlength' => '1500'
                    ]
                )}}
            </div>

            <div class="form-group">
                <h3>2.5 {{ trans('form.technical-implementation-description') }}:</h3>
                {{ Form::textarea('formData[technicalImplementation]', null,
                    [
                        'id' => 'technical-implementation', 'class' => 'form-control',
                        'placeholder' => trans('form.main-functionale-description'),
                        'maxlength' => '1500'
                    ]
                )}}
            </div>

            <div class="form-group">
                <h3>2.6 {{ trans('form.ict-tool-maintanance') }}:</h3>
                {{ Form::textarea('formData[toolFuture]', null,
                    [
                        'id' => 'tool-future', 'class' => 'form-control',
                        'placeholder' => trans('form.ict-tool-maintanance-placeholder'),
                        'maxlength' => '1500'
                    ]
                )}}
            </div>

            <div class="form-group">
                <h3>2.7 {{ trans('form.other-ict-tools') }}</h3>
                {{ Form::textarea('formData[otherTool]', null,
                    [
                        'id' => 'other-tool', 'class' => 'form-control',
                        'placeholder' => trans('form.other-ict-tools-placeholder'),
                        'maxlength' => '1500'
                    ]
                )}}
            </div>


            <div class="form-group">
                <h3>2.8 {{ trans('form.areas-of-e-development') }}</h3>
                <div class="form-check">
                    {{ Form::checkbox('formData[areasOfEdevelopment][]', 'e-Participation of citizens', false,
                        ['id' => 'e-part-of-citizens', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                    )}}
                    {{ Form::label('e-part-of-citizens', trans('form.e-participation'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::checkbox('formData[areasOfEdevelopment][]', 'e-Transparency of public sector', false,
                        ['id' => 'e-transparency', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                    )}}
                    {{ Form::label('e-transparency', trans('form.e-transparancy'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::checkbox('formData[areasOfEdevelopment][]', 'e-Government and e-Services', false,
                        ['id' => 'e-gov', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                    )}}
                    {{ Form::label('e-gov', trans('form.e-government'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::checkbox('formData[areasOfEdevelopment][]', 'Citizens mobilization', false,
                        ['id' => 'citizen-mobilization', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                    )}}
                    {{ Form::label('citizen-mobilization', trans('form.citizens-mobilization'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::checkbox('formData[areasOfEdevelopment][]', 'Citizens-to-citizens services', false,
                        ['id' => 'citizen-to-citizen', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                    )}}
                    {{ Form::label('citizen-to-citizen', trans('form.citizens-to-citizens'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::checkbox('formData[areasOfEdevelopment][]', 'CSO transparency and accountability', false,
                        ['id' => 'cso-transparency', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                    )}}
                    {{ Form::label('cso-transparency', trans('form.cso-transparency'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::checkbox('formData[areasOfEdevelopment][]', 'Other', false,
                        ['id' => 'areas-of-development-other', 'class' => 'form-check-input', 'data-show' => '#development-areas-textarea', 'data-checkbox-max' => '3']
                    )}}
                    {{ Form::label('areas-of-development-other', trans('form.other'), ['class' => 'form-check-label']) }}
                </div>
            </div>

            <div id="development-areas-textarea" class="form-group form-hidden">
                {{ Form::textarea('formData[developmentAreasTextarea]', null,
                    ['class' => 'form-control', 'placeholder' => trans('form.please-specify')]
                )}}
            </div>
        </div>

        <div class="tab">
            <div class="form-group">
                <h3>3.1 {{ trans('form.experience-of-working-or-volunteering') }}</h3>
                {{ Form::select('formData[workingExperience]', $experience_of_working, null, 
                    [
                        'id' => 'listening-comperhension',
                        'class' => 'form-control',
                        'placeholder' => trans('form.please-choose')
                    ]
                )}}
            </div>

            <div class="form-group">
                <h3>3.2 {{ trans('form.currently-working-or-volunteering') }}</h3>

                <div class="form-check">
                    {{ Form::radio('formData[currentlyWorking]', 'No, I am not engaged in any civil activity now', false,
                        ['id' => 'not-engaged-in-civil-activity', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('not-engaged-in-civil-activity', trans('form.not-engaged-in-any-civil-activity'), 
                        ['class' => 'form-check-label']
                    )}}
                </div>

                <div class="form-check">
                    {{ Form::radio('formData[currentlyWorking]', 'Yes, I am full time employed with a civil society organisation', false,
                        ['id' => 'full-employed-cso', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('full-employed-cso', trans('form.yes-full-time-employed'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::radio('formData[currentlyWorking]', 'Yes, I am part time employed with a civil society organisation', false,
                        ['id' => 'part-time-cso', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('part-time-cso', trans('form.yes-part-time-employed'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::radio('formData[currentlyWorking]', 'Yes, I am a volunteer with a civil society organisation, community or civil movement', false,
                        ['id' => 'volonteer-cso', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('volonteer-cso', trans('form.yes-i-am-volunteer'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check">
                    {{ Form::radio('formData[currentlyWorking]', 'No, I am in full-time education, please provide details', false,
                        ['id' => 'full-time-education', 'class' => 'form-check-input', 'data-show' => '#currently-working-textarea']
                    )}}
                    {{ Form::label('full-time-education', trans('form.no-full-time-education'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[currentlyWorking]', 'Other', false,
                        ['id' => 'currently-working-other', 'class' => 'form-check-input', 'data-show' => '#currently-working-textarea']
                    )}}
                    {{ Form::label('currently-working-other', trans('form.other'), ['class' => 'form-check-label']) }}
                </div>
            </div>
            <div id="currently-working-textarea" class="form-group form-hidden">
                {{ Form::textarea('formData[currentlyWorkingTextarea]', null,
                    [
                        'class' => 'form-control',
                        'placeholder' => trans('form.please-specify')
                    ]
                )}}
            </div>

            <div class="form-group">
                <h3>3.3 {{ trans('form.name-and-links-of-organisations') }}:</h3>
                {{ Form::textarea('formData[organisationsAssosicatedWith]', null, ['id' => 'organisations-associated-with', 'class' => 'form-control']) }}
            </div>

            <div class="form-group">
                <h3>3.4 {{ trans('form.position-and-how-long') }}</h3>
                {{ Form::textarea('formData[positionsAndTermsOfWorking]', null, ['id' => 'positions-and-terms-of-working', 'class' => 'form-control']) }}
            </div>

            <div class="form-group">
                <h3>3.5 {{ trans('form.founder-of-organisation') }}</h3>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[founderOfOrganisation]', 'Yes', false,
                        ['id' => 'organisation-founder-yes', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('organisation-founder-yes', trans('form.yes'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[founderOfOrganisation]', 'No', false,
                        ['id' => 'organisation-founder-no', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('organisation-founder-no', trans('form.no'), ['class' => 'form-check-label']) }}
                </div>
            </div>

            <div class="form-group">
                <h3>3.6 {{ trans('form.organisation-awareness') }}</h3>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[organisationAwareOfInterest]', 'Yes', false,
                        ['id' => 'organisation-aware-yes', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('organisation-aware-yes', trans('form.yes'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[organisationAwareOfInterest]', 'No', false,
                        ['id' => 'organisation-aware-no', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('organisation-aware-no', trans('form.no'), ['class' => 'form-check-label']) }}
                </div>
            </div>

            <div class="form-group">
                <label>{{ trans('form.letter-of-support') }}</label>
                {{ Form::file('supportLetter', ['id' => 'support-letter', 'class' => 'form-control-file']) }}
            </div>

            <div class="form-group">
                <h3>3.7 {{ trans('form.ever-been-participated') }}</h3>
                {{ Form::textarea('formData[participateItProducts]', null, ['id' => 'participate-it-products', 'class' => 'form-control']) }}
            </div>
        </div>

        <div class="tab">    

            <div class="form-group">
                <h3>4.1 {{ trans('form.do-you-have-hack-team') }}</h3>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[hackTeamWorkWith]', 'Yes', false,
                        ['id' => 'hack-team-work-with-yes', 'class' => 'form-check-input', 'data-show' => '#hack-team-members', 'data-hide' => '#hack-team-experts']
                    )}}
                    {{ Form::label('hack-team-work-with-yes', trans('form.yes'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[hackTeamWorkWith]', 'No', false,
                        ['id' => 'hack-team-work-with-no', 'class' => 'form-check-input', 'data-show' => '#hack-team-experts', 'data-hide' => '#hack-team-members']
                    )}}
                    {{ Form::label('hack-team-work-with-no', trans('form.no'), ['class' => 'form-check-label']) }}
                </div>
            </div>
            
            <div class="form-hidden" id="hack-team-members">
                <div class="form-group">
                    <label>{{ trans('form.hack-team-info') }}</label>
                </div>

                <h5>{{ trans('form.hack-team-member') }} 1:</h5>
                
                <div class="form-row">
                    <div class="form-group col-md-9">
                        {{ Form::label('first-hack-member-name', trans('form.first-and-last-name')) }}
                        {{ Form::text('formData[firstHackMemberName]', null, ['id' => 'first-hack-member-name', 'class' => 'form-control'])}}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('first-hack-member-country', trans('form.citizenship')) }}
                        {{ Form::select('formData[firstHackMemberCitizenshipCountry]', $countries, null,
                            [
                                'id' => 'first-hack-member-country',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                </div>
                               
                <div class="form-group">
                    <label>{{ trans('form.residential-address') }}:</label>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {{ Form::text('formData[firstHackMemberStreet]', null,
                                [
                                    'id' => 'first-hack-member-street',
                                    'class' => 'form-control',
                                    'placeholder' => trans('form.street-house-apartment')
                                ]
                            )}}
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::text('formData[firstHackMemberCity]', null,
                                [
                                    'id' => 'first-hack-member-city',
                                    'class' => 'form-control',
                                    'placeholder' => trans('form.city-town')
                                ]
                            )}}
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::select('formData[firstHackMemberResidetialCountry]', $countries, null,
                                [
                                    'class' => 'form-control',
                                    'placeholder' => trans('form.please-choose')
                                ]
                            )}}
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {{ Form::text('formData[firstHackMemberExpertise]', null,
                                [
                                    'class' => 'form-control',
                                    'placeholder' => trans('form.technical-expertise')
                                ]
                            )}}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::text('formData[firstHackMemberRole]', null,
                                [
                                    'class' => 'form-control',
                                    'placeholder' => trans('form.role-in-hack-team')
                                ]
                            )}}
                        </div>
                    </div>

                </div>

                <h5>{{ trans('form.hack-team-member') }} 2 ({{ trans('form.optional') }}):</h5>
                
                <div class="form-row">
                    <div class="form-group col-md-9">
                        {{ Form::label('second-hack-member-name', trans('form.first-and-last-name')) }}
                        {{ Form::text('formData[secondHackMemberName]', null, ['id' => 'second-hack-member-name', 'class' => 'form-control optional'])}}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('second-member-citizenship-country', trans('form.citizenship')) }}
                        {{ Form::select('formData[secondHackMemberCitizenshipCountry]', $countries, null,
                            [
                                'id' => 'second-member-citizenship-country',
                                'class' => 'form-control optional',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                </div>
                
                <div class="form-group">
                    <label>{{ trans('form.residential-address') }}</label>
                    <div class="form-row">   
                        <div class="form-group col-md-6">
                            {{ Form::text('formData[secondHackMemberStreet]', null,
                                [
                                    'id' => 'second-hack-member-street',
                                    'class' => 'form-control optional',
                                    'placeholder' => trans('form.street-house-apartment')
                                ]
                            )}}
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::text('formData[secondHackMemberCity]', null,
                                [
                                    'id' => 'second-hack-member-city',
                                    'class' => 'form-control optional',
                                    'placeholder' => trans('form.city-town')
                                ]
                            )}}
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::select('formData[secondHackMemberResidetialCountry]', $countries, null,
                                [
                                    'class' => 'form-control optional',
                                    'placeholder' => trans('form.please-choose')
                                ]
                            )}}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {{ Form::text('formData[secondHackMemberExpertise]', null,
                                [
                                    'class' => 'form-control optional',
                                    'placeholder' => trans('form.technical-expertise')
                                ]
                            )}}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::text('formData[secondHackMemberRole]', null,
                                [
                                    'class' => 'form-control optional',
                                    'placeholder' => trans('form.role-in-hack-team')
                                ]
                            )}}
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="form-hidden" id="hack-team-experts">

                <div class="form-group">
                    <label>{{ trans('form.select-technical-experts') }}</label>
                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Data collection and scraping experts', false,
                            ['id' => 'data-collection-expert', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('data-collection-expert', trans('form.data-collection-experts'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Data verification experts', false,
                            ['id' => 'data-verification-experts', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('data-verification-experts', trans('form.data-verification-experts'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Interactive map developers', false,
                            ['id' => 'interactive-map-developers', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('interactive-map-developers', trans('form.interactive-map-developers'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Experts on gamification', false,
                            ['id' => 'experts-on-gamification', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('experts-on-gamification', trans('form.experts-on-gamification'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Data crowd-sourcing experts', false,
                            ['id' => 'data-crowd-sourcing-experts', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('data-crowd-sourcing-experts', trans('form.data-crowd-sourcing-experts'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Mobile apps developers', false,
                            ['id' => 'mobile-apps-developers', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('mobile-apps-developers', trans('form.mobile-app-developers'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Web-developers', false,
                            ['id' => 'web-developers', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('web-developers', trans('form.web-developers'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Web-designers', false,
                            ['id' => 'web-designers', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('web-designers', trans('form.web-designers'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Illustrators', false,
                            ['id' => 'illustrators', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('illustrators', trans('form.illustrators'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Graphic designers for data visualising', false,
                            ['id' => 'data-visualising-designers', 'class' => 'form-check-input', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('data-visualising-designers', trans('form.data-visualising-graphic-designers'), ['class' => 'form-check-label']) }}
                    </div>

                    <div class="form-check">
                        {{ Form::checkbox('formData[technicalExperts][]', 'Other', false,
                            ['id' => 'technical-experts-other', 'class' => 'form-check-input', 'data-show' => '#technical-experts-textarea', 'data-checkbox-max' => '3']
                        )}}
                        {{ Form::label('technical-experts-other', trans('form.other'), ['class' => 'form-check-label']) }}
                    </div>
                </div>
                <div id="technical-experts-textarea" class="form-group form-hidden">
                    {{ Form::textarea('formData[technicalExpertsTextarea]', null,
                        ['class' => 'form-control', 'placeholder' => trans('form.please-specify')]
                    )}}
                </div>
            </div>


            <div class="form-group">
                <h3>5.1 {{ trans('form.further-personal-information') }}</h3>
                {{ Form::textarea('formData[personalInformation]', null, [ 'id' => 'personal-information', 'class' => 'form-control optional']) }}
            </div>
            
            <div class="form-group">
                <div class="form-check">
                    {{ Form::checkbox('formData[signDeclaration]', 'Signed', false,
                        ['id' => 'sing-declaration', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('sing-declaration', trans('form.application-sign'), ['class' => 'form-check-label']) }}
                </div>
            </div>
        </div>

    {{ Form::close() }}

</div>
</div>
