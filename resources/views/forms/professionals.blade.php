<div id="professionals" class="container" style="display: none;">
<div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">    
    {{ Form::open(['id' => 'professionals-form', 'route' => 'professionals.form.store', 'method' => 'post', 'files' => true]) }}
        <div class="tab">
            {{ Form::hidden('type', 'professinal') }}
            
            <div class="form-group">
                <div><label>{{ trans('form.title') }}:</label></div>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Title]', 'Mr', false,
                        ['id' => 'mr', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('mr', trans('form.mr'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Title]', 'Mrs', false,
                        ['id' => 'mrs', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('mrs', trans('form.mrs'), ['class' => 'form-check-label']) }}
                </div>
                
                @if (App::isLocale('en'))
                    <div class="form-check form-check-inline">
                        {{ Form::radio('formData[Title]', 'Ms', false,
                            ['id' => 'ms', 'class' => 'form-check-input']
                        )}}
                        {{ Form::label('ms', 'Ms', ['class' => 'form-check-label']) }}
                    </div>
                @endif
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <h3>1.1 {{ trans('form.first-name') }}:</h3>    
                    {{ Form::text('formData[firstName]', null, ['id' => 'first-name', 'class' => 'form-control'])}}
                </div>
                <div class="form-group col-md-6">
                    <h3>1.2 {{ trans('form.last-name') }}:</h3>
                    {{ Form::text('formData[lastName]', null, ['id' => 'last-name', 'class' => 'form-control'])}}
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <h3>1.3 {{ trans('form.contact-phone') }}:</h3>
                    {{ Form::text('formData[Phone]', null, ['id' => 'phone', 'class' => 'form-control'])}}
                </div>
                <div class="form-group col-md-6">
                    <h3>1.4 E-mail</h3>
                    {{ Form::email('formData[Email]', null, ['id' => 'email', 'class' => 'form-control'])}}
                </div>
            </div>

            <div class="form-group">
                <h3>1.5 {{ trans('form.residential-address') }}:</h3>
            </div>

            <div class="form-group">
                {{ Form::label('steet-house-apartment', trans('form.street-house-apartment')) }}
                {{ Form::text('formData[streetHouseApartment]', null, ['id' => 'steet-house-apartment', 'class' => 'form-control'])}}
            </div>

            <div class="form-group">
                {{ Form::label('city', trans('form.city-town')) }}
                {{ Form::text('formData[City]', null, ['id' => 'city', 'class' => 'form-control'])}}
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    {{ Form::label('residential-country', trans('form.country')) }}
                    {{ Form::select('formData[residentialCountry]', $countries, null, 
                        [
                            'id' => 'residential-country',
                            'class' => 'form-control',
                            'placeholder' => trans('form.please-choose')
                        ]
                    )}}
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <h3>1.6 {{ trans('form.citizenship') }}:</h3>
                    {{ Form::select('formData[citizenshipCountry]', $countries, null,
                        [
                            'id' => 'citizenship-country',
                            'class' => 'form-control',
                            'placeholder' => trans('form.please-choose')
                        ]
                    )}}
                </div>
            </div>

            <div class="form-group">
                <h3>1.7 {{ trans('form.gender') }}:</h3>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Gender]', 'Male', false,
                        ['id' => 'male', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('male', trans('form.male'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Gender]', 'Female', false,
                        ['id' => 'female', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('female', trans('form.female'), ['class' => 'form-check-label']) }}
                </div>
            </div>
            
            <div class="form-group">
                <h3>1.8 {{ trans('form.language-skills') }}:</h3>
            </div>

            <div class="form-row form-group">
                <div class="col-md-2 align-self-center">
                    <label class="align-self-center">{{ trans('form.russian') }}:</label>
                </div>
                <div class="col-md-10 form-row">
                    <div class="col-md-4 align-self-end">{{ Form::label('reading-ru', trans('form.reading')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('speaking-ru', trans('form.speaking')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('listening-ru', trans('form.listening-comprehension')) }}</div>
                    <div class="col-md-4">
                        {{ Form::select('formData[readingRu]', $language_levels, null, 
                            [
                                'id' => 'reading-ru',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[speakingRu]', $language_levels, null,
                            [
                                'id' => 'speaking-ru',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[listeningRu]', $language_levels, null, 
                            [
                                'id' => 'listening-ru',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                </div>
            </div>

            <div class="form-row form-group">
                <div class="col-md-2 align-self-center">
                    <label class="align-self-center">English:</label>
                </div>
                <div class="col-md-10 form-row">
                    <div class="col-md-4 align-self-end">{{ Form::label('reading-en', trans('form.reading')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('speaking-en', trans('form.speaking')) }}</div>
                    <div class="col-md-4 align-self-end">{{ Form::label('listening-en', trans('form.listening-comprehension')) }}</div>
                    <div class="col-md-4">
                        {{ Form::select('formData[readingEn]', $language_levels, null, 
                            [
                                'id' => 'reading-en',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[speakingEn]', $language_levels, null,
                            [
                                'id' => 'speaking-en',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('formData[listeningEn]', $language_levels, null, 
                            [
                                'id' => 'listening-en',
                                'class' => 'form-control',
                                'placeholder' => trans('form.please-choose')
                            ]
                        )}}
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="tab">
            <div class="form-group">
                <h3>2.1 {{ trans('form.expertise-main-area') }}</h3>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Expertise]', 'IT', false,
                        ['id' => 'it', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('it', 'IT', ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Expertise]', 'Graphic design', false,
                        ['id' => 'graphic-design', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('graphic-design', trans('form.graphic-design'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[Expertise]', 'IT and Graphic design', false,
                        ['id' => 'both', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('both', trans('form.both'), ['class' => 'form-check-label']) }}
                </div>
            </div>

            <div class="form-group">
                <h3>2.2 {{ trans('form.employment-status') }}:</h3>
                <div class="form-check">
                    {{ Form::radio('formData[employementStatus]', 'Full time employment', false,
                            ['id' => 'full-time-employment', 'class' => 'form-check-input']
                        )}}
                    {{ Form::label('full-time-employment', trans('form.full-time-employment'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[employementStatus]', 'Part time employment', false,
                        ['id' => 'part-time-employment', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('part-time-employment', trans('form.part-time-employment'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[employementStatus]', 'Freelancer', false,
                        ['id' => 'freelancer', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('freelancer', trans('form.freelancer'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[employementStatus]', 'In full-time education', false,
                        ['id' => 'full-time-education-professionals', 'class' => 'form-check-input', 'data-show' => '#employement-status-textarea']
                    )}}
                    {{ Form::label('full-time-education-professionals', trans('form.in-full-time-education'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[employementStatus]', 'Other', false,
                        ['id' => 'employement-status-other', 'class' => 'form-check-input', 'data-show' => '#employement-status-textarea']
                    )}}
                    {{ Form::label('employement-status-other', trans('form.other'), ['class' => 'form-check-label']) }}
                </div>
            </div>

            <div id="employement-status-textarea" class="form-group form-hidden">
                {{ Form::textarea('formData[otherEmployement]', null,
                    [
                        'id' => 'other-employement', 'class' => 'form-control',
                        'placeholder' => trans('form.please-specify'),
                    ]
                )}}
            </div>

            <div class="form-group">
                <h3>2.3 {{ trans('form.working-in-it-industry-term') }}</h3>
                <div class="form-check">
                    {{ Form::radio('formData[workExperience]', '0 - 2 years', false,
                            ['id' => 'zero-two', 'class' => 'form-check-input']
                        )}}
                    {{ Form::label('zero-two', trans('form.0-2-years'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[workExperience]', '2 - 5 years', false,
                            ['id' => 'two-five', 'class' => 'form-check-input']
                        )}}
                    {{ Form::label('two-five', trans('form.2-5-years'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[workExperience]', '6 - 8 years', false,
                            ['id' => 'six-eight', 'class' => 'form-check-input']
                        )}}
                    {{ Form::label('six-eight', trans('form.6-8-years'), ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('formData[workExperience]', 'over 8 years', false,
                            ['id' => 'over-eight', 'class' => 'form-check-input']
                        )}}
                    {{ Form::label('over-eight', trans('form.over-8-years'), ['class' => 'form-check-label']) }}
                </div>
            </div>

            <div class="form-group">
                <h3>2.4 {{ trans('form.areas-of-expertise') }}</h3>
                {{ Form::textarea('formData[linksToProject]', null, ['id' => 'links-to-project', 'class' => 'form-control', 'placeholder' => trans('form.areas-of-expertise-placeholder')]) }}
            </div>

            <div class="form-group">
                <h3>2.5 {{ trans('form.website-portfolio') }}</h3>
                {{ Form::text('formData[linkToPortfolio]', null, ['id' => 'link-to-portfolio', 'class' => 'form-control']) }}
            </div>

        </div>
        
        <div class="tab">
            <div class="form-group">
                <h3>4.1 {{ trans('form.experience-of-working-with-civil-society-organisations') }}</h3>
                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[previousExperience]', 'Yes', false,
                        ['id' => 'professional-previous-experience-yes', 'class' => 'form-check-input', 'data-show' => '#previous-experience-textarea']
                    )}}
                    {{ Form::label('professional-previous-experience-yes', trans('form.yes'), ['class' => 'form-check-label']) }}
                </div>

                <div class="form-check form-check-inline">
                    {{ Form::radio('formData[previousExperience]', 'No', false,
                        ['id' => 'professional-previous-experience-no', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('professional-previous-experience-no', trans('form.no'), ['class' => 'form-check-label']) }}
                </div>
            </div>

            <div id="previous-experience-textarea" class="form-group form-hidden">
                {{ Form::textarea('formData[previousExperienceTextarea]', null,
                    [
                        'id' => 'links-to-project', 'class' => 'form-control', 
                        'placeholder' => trans('form.details-of-projects-and-results')
                    ]
                )}}
            </div>

            <div class="form-group">
                <h3>4.2 {{ trans('form.participate-reason') }}</h3>
                {{ Form::textarea('formData[whyParticipateBrief]', null, ['id' => 'why-participate-brief', 'class' => 'form-control']) }}
            </div>

            <div class="form-group">
                <h3>5.1 {{ trans('form.further-personal-information') }}:</h3>
                {{ Form::textarea('formData[personalInformation]', null, 
                    [
                        'id' => 'why-participate-brief', 'class' => 'form-control',
                    ]
                )}}
            </div>

            <div class="form-group">
                <div class="form-check">
                    {{ Form::checkbox('formData[signDeclaration]', 'Signed', false,
                        ['id' => 'professional-sing-declaration', 'class' => 'form-check-input']
                    )}}
                    {{ Form::label('professional-sing-declaration', trans('form.application-sign'), ['class' => 'form-check-label']) }}
                </div>
            </div>
        </div>
        {{-- {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }} --}}
    {{ Form::close() }}

</div>
</div>