@extends('layouts.admin')

@section('title')
    Правила и FAQ
@endsection

@section('header')
    Правила и FAQ
@endsection

@section('content')
<div class="col-lg-3">
    <div class="form-group create-button">
        <a href="{{ route('rules.create') }}" class="btn btn-outline-primary">Создать запись</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Заголовок</th>
            {{-- <th>Текст</th> --}}
            <th>Операция</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($posts as $post)
                <tr>
                    <td>{{ $post->title }}</td>
                    {{-- <td>{{ str_limit($post->content, 120) }}</td> --}}
                    <td>
                        {{ Form::open(['route' => ['rules.destroy', $post], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('rules.edit', $post) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
