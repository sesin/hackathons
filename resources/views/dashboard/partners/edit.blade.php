@extends('layouts.admin')

@section('title')
    Редактирование партнера {{ $partner->caption }}
@endsection

@section('header')
    Редактирование партнера {{ $partner->caption }}
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class='col-lg-8'>

    {{ Form::model($partner, ['route' => ['partners.update', $partner->id], 'method' => 'PUT']) }}

        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#ru" role="tab">Русский язык</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#en" role="tab">Английский язык</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#meta" role="tab">Мета-даные</a>
            </li>
        </ul>

        <div class="tab-content pt-3">
        <!-- Русская версия -->
            <div class="tab-pane fade show active" id="ru" role="tabpanel">
                <div class="form-group">
                    {{ Form::label('caption_ru', 'Надпись') }}
                    {{ Form::text('caption_ru', null, ['class' => 'form-control' . ($errors->has('caption_ru') ? ' is-invalid' : '')]) }}

                    @if($errors->has('caption_ru'))
                        @foreach ($errors->get('caption_ru') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- Конец русской версии -->

            <!-- Английская версия -->
            <div class="tab-pane fade" id="en" role="tabpanel">
                <div class="form-group">
                    {{ Form::label('caption_en', 'Надпись') }}
                    {{ Form::text('caption_en', null, ['class' => 'form-control' . ($errors->has('caption_en') ? ' is-invalid' : '')]) }}

                    @if($errors->has('caption_en'))
                        @foreach ($errors->get('caption_en') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- Конец английской версии -->

            <!-- Мета-данные -->
            <div class="tab-pane fade" id="meta" role="tabpanel">
                <div class="form-group">
                    {{ Form::label('link', 'Cсылка') }}
                    {{ Form::text('link', null, ['class' => 'form-control' . ($errors->has('link') ? ' is-invalid' : '')]) }}

                    @if($errors->has('link'))
                        @foreach ($errors->get('link') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>

                <div class="form-group">
                    {{ Form::label('logo', 'URL лого') }}
                    <div class="input-group mb-3">
                        {{ Form::text('logo', null, ['id' => 'logo','class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : '')]) }}
                        <div class="input-group-append">
                            <button data-toggle="modal" href="javascript:;" data-target="#images" class="btn btn-outline-primary" type="button">Выбрать лого</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Конец мета-данных -->
        </div>

        {{ Form::submit('Обновить', ['class' => 'btn btn-outline-primary']) }}

    {{ Form::close() }}

</div>

@endsection