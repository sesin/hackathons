@extends('layouts.admin')

@section('title')
    Партнеры
@endsection

@section('header')
    Партнеры
@endsection

@section('content')
<div class="col-lg-3">
    <div class="form-group create-button">
        <a href="{{ route('partners.create') }}" class="btn btn-outline-primary">Создать запись</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Надпись</th>
            <th>Ссылка</th>
            <th>Операция</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($partners as $partner)
                <tr>
                    <td>{{ $partner->caption }}</td>
                    <td>{{ $partner->link }}</td>
                    <td>
                        {{ Form::open(['route' => ['partners.destroy', $partner], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('partners.edit', $partner) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
