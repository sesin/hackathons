@extends('layouts.admin-without-sidebar')

@section('content')
        <div class="card w-50 m-auto mt-5">
            <div class="card-body">
                <h3 class="card-title">Confirm delete</h3>
                <p class="card-text">
                    После того как вы нажмете "Удалить" альбом "{{ $album->name }}" будет удален,
                    и вы не сможете восстановить его в будущем.
                </p>
                {{ Form::open(['route' => ['albums.destroy', $album], 'method' => 'delete', 'style' => 'display: inline-block']) }}
                    {{ Form::hidden('confirmed', true) }}
                    <button class="btn btn-danger">Удалить</button>
                {{ Form::close() }}
                <a class="btn btn-secondary" href="{{ route('albums.index') }}">Отмена</a>
            </div>
        </div>
@endsection
