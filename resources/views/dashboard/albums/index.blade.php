@extends('layouts.admin')

@section('title')
    Как это было в 2017
@endsection

@section('header')
    Как это было в 2017
@endsection

@section('content')
<div class="mb-4">
    <a href="{{ route('albums.create') }}" class="btn btn-outline-primary">Создать альбом</a>
    <a href="{{ route('album-items.index') }}" class="btn btn-outline-primary">Изменение/добавление элменетов</a>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Обложка</th>
            <th>Название</th>
            <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($albums as $album)
                <tr>
                    <td><img src="/thumbs/{{$album->cover_image_path}}"></td>
                    <td class="text-center">{{ $album->name }}</td>
                    <td class="text-center">
                        {{ Form::open(['route' => ['albums.destroy', $album], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('albums.edit', $album) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
