@extends('layouts.admin')

@section('title')
    Добавить Альбом
@endsection

@section('header')
    Добавить Альбом
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{ Form::open(['route' => 'albums.index']) }}


        <div class="tab-pane fade show active" id="data" role="tabpanel">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Русский язык</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Английский язык</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#contact" role="tab">Мета-даные</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#elements" role="tab">Элементы</a>
                </li>
            </ul>

            <div class="tab-content pt-3">
                <!-- Русская версия -->
                <div class="tab-pane fade show active" id="home" role="tabpanel">
                    <div class="form-group">
                        {{ Form::label('name_ru', 'Название рус.') }}
                        {{ Form::text('name_ru', null, ['class' => 'form-control' . ($errors->has('name_ru') ? ' is-invalid' : '')]) }}
                
                        @if($errors->has('name_ru'))
                            @foreach ($errors->get('name_ru') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- Конец русской версии -->

                <!-- Английская версия -->
                <div class="tab-pane fade" id="profile" role="tabpanel">
                    <div class="form-group">
                        {{ Form::label('name_en', 'Название анг.') }}
                        {{ Form::text('name_en', null, ['class' => 'form-control' . ($errors->has('name_en') ? ' is-invalid' : '')]) }}

                        @if($errors->has('name_en'))
                            @foreach ($errors->get('name_en') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- Конец английской версии -->

                <!-- Мета-данные -->
                <div class="tab-pane fade" id="contact" role="tabpanel">
                    <div class="form-group">
                        {{ Form::label('cover_image_path', 'Обложка') }}
                        <div class="input-group mb-3">
                            {{ Form::text('cover_image_path', null, ['id' => 'cover_image_path','class' => 'form-control' . ($errors->has('cover_image_path') ? ' is-invalid' : '')]) }}
                            <div class="input-group-append">
                                <button data-toggle="modal" href="javascript:;" data-target="#images" class="btn btn-outline-primary" type="button">Выбрать фото</button>
                            </div>
                        </div>

                        @if($errors->has('cover_image_path'))
                            @foreach ($errors->get('cover_image_path') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- Конец мета-данных -->
                
                <!-- Элементы -->
                <div class="tab-pane fade" id="elements" role="tabpanel">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Миниатюра</th>
                                <th>Ссылка/путь</th>
                                <th>Добавить</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($album_items as $album_item)
                                    <tr>
                                        <td><img src="{{ $album_item->small_image }}"></td>
                                        <td>{{ $album_item->url }}</td>
                                        <td>
                                            {{ Form::checkbox('album_items[]', $album_item->id, false) }}
                                        </td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- Конец лементов -->

            </div>

        </div>





    {{ Form::submit('Добавить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}

<div id="images" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <iframe width="800" height="600" src="/filemanager/dialog.php?type=1&field_id=cover_image_path&relative_url=1&akey=EGDT3dyjsMFxYV865EasV6e5RHkRXDCas8r7t2mv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
        </div>
    </div>
</div>

@endsection