@extends('layouts.admin')

@section('title')
    Элементы альбомов
@endsection

@section('header')
    Элементы альбомов
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-md-6">
    {{ Form::open(['route' => 'album-items.upload-images', 'method' => 'post', 'class' => 'form-inline', 'files' => true]) }}

        <div class="form-group">
            {{ Form::label('upload-images', 'Загрузить картинки') }}
            {{ Form::file('image', ['id' => 'upload-images', 'class' => 'form-control-file mb-4']) }}
        </div>

        <button type="submit" class="btn btn-primary mb-2">Загрузить</button>
    {{ Form::close() }}

    {{ Form::open(['route' => 'album-items.index', 'class' => 'form-inline',]) }}

        {{ Form::text('url', null, ['id' => 'url', 'class' => 'form-control mb-2 mr-5 col-md-6', 'placeholder' => 'URL']) }}

        <button type="submit" class="btn btn-primary mb-2 ml-3">Добавить</button>

    {{ Form::close() }}

    <div class="mb-4">
        <a href="{{ route('albums.create') }}" class="btn btn-outline-primary">Создать альбом</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Миниатюра</th>
            <th>Ссылка/путь</th>
            <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($album_items as $album_item)
                <tr>
                    <td><img src="{{ $album_item->small_image }}"></td>
                    <td>{{ $album_item->url }}</td>
                    <td>
                        {{ Form::open(['route' => ['album-items.destroy', $album_item], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('album-items.edit', $album_item) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
