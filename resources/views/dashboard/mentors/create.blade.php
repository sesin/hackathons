@extends('layouts.admin')

@section('title')
    Добавить ментора
@endsection

@section('header')
    Добавить ментора
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{ Form::open(['route' => 'mentors.index']) }}
    
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Русский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Английский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#contact" role="tab">Мета-даные</a>
        </li>
    </ul>

    <div class="tab-content pt-3">
        <!-- Русская версия -->
        <div class="tab-pane fade show active" id="home" role="tabpanel">
            <div class="form-group">
                {{ Form::label('name_ru', 'Имя рус.') }}
                {{ Form::text('name_ru', null, ['class' => 'form-control' . ($errors->has('name_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('name_ru'))
                    @foreach ($errors->get('name_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
            
            <div class="form-group">
                {{ Form::label('profession_ru', 'Профессия рус.') }}
                {{ Form::text('profession_ru', null, ['class' => 'form-control' . ($errors->has('profession_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('profession_ru'))
                    @foreach ($errors->get('profession_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_one_ru', 'Умение 1 рус.') }}
                {{ Form::text('skill_one_ru', null, ['class' => 'form-control' . ($errors->has('skill_one_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('skill_one_ru'))
                    @foreach ($errors->get('skill_one_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_two_ru', 'Умение 2 рус.') }}
                {{ Form::text('skill_two_ru', null, ['class' => 'form-control' . ($errors->has('skill_two_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('skill_two_ru]'))
                    @foreach ($errors->get('skill_two_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_three_ru', 'Умение 3 рус.') }}
                {{ Form::text('skill_three_ru', null, ['class' => 'form-control' . ($errors->has('skill_three_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('skill_three_ru'))
                    @foreach ($errors->get('skill_three_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('description_ru', 'Текст рус.') }}
                {{ Form::textarea('description_ru', null, ['class' => 'form-control tinymce-val' . ($errors->has('description_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('description_ru'))
                    @foreach ($errors->get('description_ru') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец русской версии -->

        <!-- Английская версия -->
        <div class="tab-pane fade" id="profile" role="tabpanel">
            <div class="form-group">
                {{ Form::label('name_en', 'Имя анг.') }}
                {{ Form::text('name_en', null, ['class' => 'form-control' . ($errors->has('name_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('name_en'))
                    @foreach ($errors->get('name_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('profession_en', 'Профессия анг.') }}
                {{ Form::text('profession_en', null, ['class' => 'form-control' . ($errors->has('profession_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('profession_en'))
                    @foreach ($errors->get('profession_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_one_en', 'Умение 1 анг.') }}
                {{ Form::text('skill_one_en', null, ['class' => 'form-control' . ($errors->has('skill_one_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('skill_one_en'))
                    @foreach ($errors->get('skill_one_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_two_en', 'Умение 2 анг.') }}
                {{ Form::text('skill_two_en', null, ['class' => 'form-control' . ($errors->has('skill_two_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('skill_two_en'))
                    @foreach ($errors->get('skill_two_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_three_en', 'Умение 3 анг.') }}
                {{ Form::text('skill_three_en', null, ['class' => 'form-control' . ($errors->has('skill_three_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('skill_three_en'))
                    @foreach ($errors->get('skill_three_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('description_en', 'Описание анг.') }}
                {{ Form::textarea('description_en', null, ['class' => 'form-control tinymce-val' . ($errors->has('description_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('description_en'))
                    @foreach ($errors->get('description_en') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец английской версии -->

        <!-- Мета-данные -->
        <div class="tab-pane fade" id="contact" role="tabpanel">
            <div class="form-group">
                {{ Form::label('image_path', 'URL Фото') }}
                <div class="input-group mb-3">
                    {{ Form::text('image_path', null, ['id' => 'image_path','class' => 'form-control' . ($errors->has('image_path') ? ' is-invalid' : '')]) }}
                    <div class="input-group-append">
                        <button data-toggle="modal" href="javascript:;" data-target="#images" class="btn btn-outline-primary" type="button">Выбрать фото</button>
                    </div>
                </div>

                @if($errors->has('image_path'))
                    @foreach ($errors->get('image_path') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('twitter_url', 'Twitter') }}
                {{ Form::text('twitter_url', null, ['class' => 'form-control' . ($errors->has('twitter_url') ? ' is-invalid' : '')]) }}

                @if($errors->has('twitter_url'))
                    @foreach ($errors->get('twitter_url') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('facebook_url', 'Facebook') }}
                {{ Form::text('facebook_url', null, ['class' => 'form-control' . ($errors->has('facebook_url') ? ' is-invalid' : '')]) }}

                @if($errors->has('facebook_url'))
                    @foreach ($errors->get('facebook_url') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('linkedin_url', 'LinkedIn') }}
                {{ Form::text('linkedin_url', null, ['class' => 'form-control' . ($errors->has('linkedin_url') ? ' is-invalid' : '')]) }}

                @if($errors->has('linkedin_url'))
                    @foreach ($errors->get('linkedin_url') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_one_percentage', 'Уровень первого умения') }}
                <div class="input-group">
                    {{ Form::text('skill_one_percentage', null, ['class' => 'form-control col-1 text-right' . ($errors->has('skill_one_percentage') ? ' is-invalid' : '')]) }}
                    <div class="input-group-append">
                        <span class="input-group-text">%</span>
                    </div>
                </div>

                @if($errors->has('skill_one_percentage'))
                    @foreach ($errors->get('skill_one_percentage') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_two_percentage', 'Уровень второго умения') }}
                <div class="input-group">
                    {{ Form::text('skill_two_percentage', null, ['class' => 'form-control col-1 text-right' . ($errors->has('skill_two_percentage') ? ' is-invalid' : '')]) }}
                    <div class="input-group-append">
                        <span class="input-group-text">%</span>
                    </div>
                </div>

                @if($errors->has('skill_two_percentage'))
                    @foreach ($errors->get('skill_two_percentage') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('skill_three_percentage', 'Уровень третьего умения') }}
                <div class="input-group">
                    {{ Form::text('skill_three_percentage', null, ['class' => 'form-control col-1 text-right' . ($errors->has('skill_three_percentage') ? ' is-invalid' : '')]) }}
                    <div class="input-group-append">
                        <span class="input-group-text">%</span>
                    </div>
                </div>

                @if($errors->has('skill_three_percentage'))
                    @foreach ($errors->get('skill_three_percentage') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец мета-данных -->
    </div>

    {{ Form::submit('Добавить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}

<div id="images" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <iframe width="800" height="600" src="/filemanager/dialog.php?type=1&field_id=image_path&relative_url=1&akey=EGDT3dyjsMFxYV865EasV6e5RHkRXDCas8r7t2mv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
        </div>
    </div>
</div>

@endsection