@extends('layouts.admin')

@section('title')
    Менторы
@endsection

@section('header')
    Менторы
@endsection

@section('content')
<div class="col-lg-3">
    <div class="form-group create-button">
        <a href="{{ route('mentors.create') }}" class="btn btn-outline-primary">Создать запись</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Имя</th>
            <th>Операция</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($mentors as $mentor)
                <tr>
                    <td>{{ $mentor->name }}</td>
                    <td>
                        {{ Form::open(['route' => ['mentors.destroy', $mentor], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('mentors.edit', $mentor) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
