@extends('layouts.admin')

@section('title')
    Контакты
@endsection

@section('header')
    Контакты
@endsection

@section('content')

{{ Form::model($contacts_model, ['route' => ['contacts.update', $contacts_model->id], 'method' => 'POST']) }}

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#ru" role="tab">Русский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#en" role="tab">Английский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#meta" role="tab">Мета-даные</a>
        </li>
    </ul>

    <div class="tab-content pt-3">
        <!-- Русская версия -->
        <div class="tab-pane fade show active" id="ru" role="tabpanel">
            <div class="form-group">
                {{ Form::label('translated_contacts_ru[main_text]', 'Главный текст рус.') }}
                {{ Form::textarea('translated_contacts_ru[main_text]',
                    $contacts_model->translate('ru')->translated_contacts->main_text, 
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_contacts_ru[main_text]') ? ' is-invalid' : '')]
                )}}
        
                @if($errors->has('translated_contacts_ru[main_text]'))
                    @foreach ($errors->get('translated_contacts_ru[main_text]') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_contacts_ru[button_text]', 'Текст кнопки рус.') }}
                {{ Form::text('translated_contacts_ru[button_text]',
                    $contacts_model->translate('ru')->translated_contacts->button_text,
                    ['class' => 'form-control' . ($errors->has('translated_contacts_ru[button_text]') ? ' is-invalid' : '')]
                )}}

                @if($errors->has('translated_contacts_ru[button_text]'))
                    @foreach ($errors->get('translated_contacts_ru[button_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец русской версии -->

        <!-- Английская версия -->
        <div class="tab-pane fade" id="en" role="tabpanel">
            <div class="form-group">
                {{ Form::label('translated_contacts_en[main_text]', 'Главный текст анг.') }}
                {{ Form::textarea('translated_contacts_en[main_text]',
                    $contacts_model->translate('en')->translated_contacts->main_text,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_contacts_en[main_text]') ? ' is-invalid' : '')]
                )}}
        
                @if($errors->has('translated_contacts_en[main_text]'))
                    @foreach ($errors->get('translated_contacts_en[main_text]') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_contacts_en[button_text]', 'Текст кнопки анг.') }}
                {{ Form::text('translated_contacts_en[button_text]',
                    $contacts_model->translate('en')->translated_contacts->button_text,
                    ['class' => 'form-control' . ($errors->has('translated_contacts_en[button_text]') ? ' is-invalid' : '')]) }}

                @if($errors->has('translated_contacts_en[button_text]'))
                    @foreach ($errors->get('translated_contacts_en[button_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец английской версии -->

        <!-- Мета-данные -->
        <div class="tab-pane fade" id="meta" role="tabpanel">
            <div class="form-group">
                {{ Form::label('contacts[button_url]', 'URL кнопки') }}
                {{ Form::text('contacts[button_url]', null, ['class' => 'form-control' . ($errors->has('contacts[button_url]') ? ' is-invalid' : '')]) }}

                @if($errors->has('contacts[button_url]'))
                    @foreach ($errors->get('contacts[button_url]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>

        {{-- <div class="tab-pane fade" id="meta" role="tabpanel">
            <div class="form-group">
                {{ Form::label('contacts[map_url]', 'URL карты') }}
                {{ Form::text('contacts[map_url]', null, ['class' => 'form-control' . ($errors->has('contacts[map_url]') ? ' is-invalid' : '')]) }}

                @if($errors->has('contacts[map_url]'))
                    @foreach ($errors->get('contacts[map_url]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div> --}}
        <!-- Конец мета-данных -->
    </div>

    {{ Form::submit('Обновить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}
@endsection