@extends('layouts.admin')

@section('content')

{{ Form::model($settings_model, ['route' => ['dashboard.update-settings', $settings_model->id], 'method' => 'POST']) }}

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#ru" role="tab">Русский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#en" role="tab">Английский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#meta" role="tab">Мета-даные</a>
        </li>
    </ul>

    <div class="tab-content pt-3">
        <!-- Русская версия -->
        <div class="tab-pane fade show active" id="ru" role="tabpanel">
            <div class="form-group">
                {{ Form::label('translated_settings_ru[banner_title]', 'Заголовок на баннере рус.') }}
                {{ Form::textarea('translated_settings_ru[banner_title]',
                    $settings_model->translate('ru')->translated_settings->banner_title, 
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_ru[banner_title]') ? ' is-invalid' : '')]
                )}}
        
                @if($errors->has('translated_settings_ru[banner_title]'))
                    @foreach ($errors->get('translated_settings_ru[banner_title]') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_ru[banner_text]', 'Текст баннера рус.') }}
                {{ Form::textarea('translated_settings_ru[banner_text]',
                    $settings_model->translate('ru')->translated_settings->banner_text,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_ru[banner_text]') ? ' is-invalid' : '')]
                )}}

                @if($errors->has('translated_settings_ru[banner_text]'))
                    @foreach ($errors->get('translated_settings_ru[banner_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_ru[about_hackathon_announcement]', 'Анонс "О хакатоне" рус.') }}
                {{ Form::textarea('translated_settings_ru[about_hackathon_announcement]',
                    $settings_model->translate('ru')->translated_settings->about_hackathon_announcement,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_ru[about_hackathon_announcement]') ? ' is-invalid' : '')]
                   )}}

                @if($errors->has('translated_settings_ru[about_hackathon_announcement]'))
                    @foreach ($errors->get('translated_settings_ru[about_hackathon_announcement]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_ru[about_hackathon_text]', 'Текст "О хакатоне" рус.') }}
                {{ Form::textarea('translated_settings_ru[about_hackathon_text]',
                    $settings_model->translate('ru')->translated_settings->about_hackathon_text,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_ru[about_hackathon_text]') ? ' is-invalid' : '')]
                   )}}

                @if($errors->has('translated_settings_ru[about_hackathon_text]'))
                    @foreach ($errors->get('translated_settings_ru[about_hackathon_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_ru[button_text]', 'Текст кнопки на баннере рус.') }}
                {{ Form::text('translated_settings_ru[button_text]',
                    $settings_model->translate('ru')->translated_settings->button_text,
                    ['class' => 'form-control' . ($errors->has('translated_settings_ru[about_hackathon_text]') ? ' is-invalid' : '')]
                )}}

                @if($errors->has('translated_settings_ru[about_hackathon_text]'))
                    @foreach ($errors->get('translated_settings_ru[about_hackathon_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('rules_file_path_ru', 'Файл правил рус.') }}
                <div class="input-group mb-3">
                    {{ Form::text('translated_settings_ru[rules_file_path]',
                        $settings_model->translate('ru')->translated_settings->rules_file_path,
                        ['id' => 'rules_file_path_ru', 'class' => 'form-control' . ($errors->has('settings[rules_file_path_ru]') ? ' is-invalid' : '')]) }}
                    <div class="input-group-append">
                        <button data-toggle="modal" href="javascript:;" data-target="#instructions_ru" class="btn btn-outline-primary" type="button">Выбрать файл</button>
                    </div>
                </div>

                @if($errors->has('settings[rules_file_path_ru]'))
                    @foreach ($errors->get('settings[rules_file_path_ru]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец русской версии -->

        <!-- Английская версия -->
        <div class="tab-pane fade" id="en" role="tabpanel">
            <div class="form-group">
                {{ Form::label('translated_settings_en[banner_title]', 'Заголовок на баннере анг.') }}
                {{ Form::textarea('translated_settings_en[banner_title]',
                    $settings_model->translate('en')->translated_settings->banner_title,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_en[banner_title]') ? ' is-invalid' : '')]
                )}}
        
                @if($errors->has('translated_settings_en[banner_title]'))
                    @foreach ($errors->get('translated_settings_en[banner_title]') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_en[banner_text]', 'Текст баннера анг.') }}
                {{ Form::textarea('translated_settings_en[banner_text]',
                    $settings_model->translate('en')->translated_settings->banner_text,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_en[banner_text]') ? ' is-invalid' : '')]
                )}}

                @if($errors->has('translated_settings_en[banner_text]'))
                    @foreach ($errors->get('translated_settings_en[banner_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_en[about_hackathon_announcement]', 'Анонс "О хакатоне" анг.') }}
                {{ Form::textarea('translated_settings_en[about_hackathon_announcement]',
                    $settings_model->translate('en')->translated_settings->about_hackathon_announcement,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_en[about_hackathon_announcement]') ? ' is-invalid' : '')]
                )}}

                @if($errors->has('translated_settings_en[about_hackathon_announcement]'))
                    @foreach ($errors->get('translated_settings_en[about_hackathon_announcement]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_en[about_hackathon_text]', 'Текст "О хакатоне" анг.') }}
                {{ Form::textarea('translated_settings_en[about_hackathon_text]',
                    $settings_model->translate('en')->translated_settings->about_hackathon_text,
                    ['class' => 'form-control tinymce-val' . ($errors->has('translated_settings_en[about_hackathon_text]') ? ' is-invalid' : '')]
                )}}

                @if($errors->has('translated_settings_en[about_hackathon_text]'))
                    @foreach ($errors->get('translated_settings_en[about_hackathon_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('translated_settings_en[button_text]', 'Текст кнопки на баннере анг.') }}
                {{ Form::text('translated_settings_en[button_text]',
                    $settings_model->translate('en')->translated_settings->button_text,
                    ['class' => 'form-control' . ($errors->has('translated_settings_en[button_text]') ? ' is-invalid' : '')]) }}

                @if($errors->has('translated_settings_en[button_text]'))
                    @foreach ($errors->get('translated_settings_en[button_text]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('rules_file_path_en', 'Файл правил анг.') }}
                <div class="input-group mb-3">
                    {{ Form::text('translated_settings_en[rules_file_path]',
                        $settings_model->translate('en')->translated_settings->rules_file_path,
                        ['id' => 'rules_file_path_en', 'class' => 'form-control' . ($errors->has('settings[rules_file_path_en]') ? ' is-invalid' : '')]) }}
                    <div class="input-group-append">
                        <button data-toggle="modal" href="javascript:;" data-target="#instructions_en" class="btn btn-outline-primary" type="button">Выбрать файл</button>
                    </div>
                </div>

                @if($errors->has('settings[rules_file_path_en]'))
                    @foreach ($errors->get('settings[rules_file_path_en]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец английской версии -->

        <!-- Мета-данные -->
        <div class="tab-pane fade" id="meta" role="tabpanel">
            <div class="form-group">
                {{ Form::label('banner_image_path', 'Изображение баннера') }}
                <div class="input-group mb-3">
                    {{ Form::text('settings[banner_image_path]', null, ['id' => 'banner_image_path', 'class' => 'form-control' . ($errors->has('settings[banner_image_path]') ? ' is-invalid' : '')]) }}
                    <div class="input-group-append">
                        <button data-toggle="modal" href="javascript:;" data-target="#images" class="btn btn-outline-primary" type="button">Выбрать фото</button>
                    </div>
                </div>

                @if($errors->has('settings[banner_image_path]'))
                    @foreach ($errors->get('settings[banner_image_path]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('settings[button_url]', 'URL кнопки') }}
                {{ Form::text('settings[button_url]', null, ['class' => 'form-control' . ($errors->has('settings[button_url]') ? ' is-invalid' : '')]) }}

                @if($errors->has('settings[button_url]'))
                    @foreach ($errors->get('settings[button_url]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('settings[emails]', 'E-mails, через запятую') }}
                {{ Form::textarea('settings[emails]', null,
                    ['class' => 'form-control' . ($errors->has('settings[emails]') ? ' is-invalid' : ''), 'placeholder' => 'mail@domain.ru, mail2@domain.en']
                )}}

                @if($errors->has('settings[emails]'))
                    @foreach ($errors->get('settings[emails]') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец мета-данных -->
    </div>

    {{ Form::submit('Обновить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}

<div id="images" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <iframe width="800" height="600" src="/filemanager/dialog.php?type=1&field_id=banner_image_path&relative_url=1&akey=EGDT3dyjsMFxYV865EasV6e5RHkRXDCas8r7t2mv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
        </div>
    </div>
</div>

<div id="instructions_ru" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <iframe width="800" height="600" src="/filemanager/dialog.php?type=2&field_id=rules_file_path_ru&relative_url=1&akey=EGDT3dyjsMFxYV865EasV6e5RHkRXDCas8r7t2mv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
        </div>
    </div>
</div>

<div id="instructions_en" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <iframe width="800" height="600" src="/filemanager/dialog.php?type=2&field_id=rules_file_path_en&relative_url=1&akey=EGDT3dyjsMFxYV865EasV6e5RHkRXDCas8r7t2mv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
        </div>
    </div>
</div>


@endsection