@extends('layouts.admin')

@section('title')
    Редактирование леции {{ $lecture->title }}
@endsection

@section('header')
    Редактирование леции {{ $lecture->title }}
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{ Form::model($lecture, ['route' => ['lectures.update', $lecture->id], 'method' => 'PUT']) }}

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#ru" role="tab">Русский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#en" role="tab">Английский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#meta" role="tab">Мета-даные</a>
        </li>
    </ul>

    <div class="tab-content pt-3">
        <!-- Русская версия -->
        <div class="tab-pane fade show active" id="ru" role="tabpanel">
            <div class="form-group">
                {{ Form::label('title_ru', 'Заголовок рус.') }}
                {{ Form::text('title_ru', old('title_ru', $lecture->translate('ru')->title), ['class' => 'form-control' . ($errors->has('title_ru') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('title_ru'))
                    @foreach ($errors->get('title_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('place_ru', 'Место рус.') }}
                {{ Form::text('place_ru', old('place_ru', $lecture->translate('ru')->place), ['class' => 'form-control' . ($errors->has('place_ru') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('place_ru'))
                    @foreach ($errors->get('place_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('content_ru', 'Текст.рус') }}
                {{ Form::textarea('content_ru', old('content_ru', $lecture->translate('ru')->content), ['class' => 'form-control tinymce-val' . ($errors->has('content_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('content_ru'))
                    @foreach ($errors->get('content_ru') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец русской версии -->    
        
        <!-- Английская версия -->
        <div class="tab-pane fade" id="en" role="tabpanel">
            <div class="form-group">
                {{ Form::label('title_en', 'Заголовок анг.') }}
                {{ Form::text('title_en', old('title_en', $lecture->translate('en')->title), ['class' => 'form-control' . ($errors->has('title_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('title_en'))
                    @foreach ($errors->get('title_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('place_en', 'Место анг.') }}
                {{ Form::text('place_en', old('place_en', $lecture->translate('en')->place), ['class' => 'form-control' . ($errors->has('place_en') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('place_en'))
                    @foreach ($errors->get('place_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('content_en', 'Текст.анг') }}
                {{ Form::textarea('content_en', old('content_en', $lecture->translate('en')->content), ['class' => 'form-control tinymce-val' . ($errors->has('content_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('content_en'))
                    @foreach ($errors->get('content_en') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец английской версии -->
        
        <!-- Мета-данные -->
        <div class="tab-pane fade" id="meta" role="tabpanel">
            <div class="form-group">
                {{ Form::label('start_at', 'Время начала') }}
                {{ Form::time('start_at', old('start_at'), ['class' => 'form-control' . ($errors->has('start_at') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('start_at'))
                    @foreach ($errors->get('start_at') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('end_at', 'Время окончания') }}
                {{ Form::time('end_at', old('end_at'), ['class' => 'form-control' . ($errors->has('end_at') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('end_at'))
                    @foreach ($errors->get('end_at') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('day_id', 'Дата') }}
                {{ Form::select('day_id', $days, $lecture->day_id, ['class' => 'form-control' . ($errors->has('end_at') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('end_at'))
                    @foreach ($errors->get('end_at') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
            
            @if($mentors)
                <h2>Наставники</h2>
                @foreach($mentors as $mentor)
                    <div class="form-check">
                        {{ Form::checkbox('mentors[]', $mentor->id,
                            in_array($mentor->id, $lecture->mentors->pluck('id')->toArray()),
                            ['id' => 'mentor-' . $mentor->id,'class' => 'form-check-input' . ($errors->has('end_at') ? ' is-invalid' : '')]) 
                        }}
                        {{ Form::label('mentor-' . $mentor->id, $mentor->name), ['class' => 'form-check-label'] }}
                    </div>
                @endforeach
            @endif

        </div>
        <!-- Конец мета-данных -->
    </div>

    {{ Form::submit('Обновить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}

@endsection