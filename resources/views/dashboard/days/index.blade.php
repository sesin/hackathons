@extends('layouts.admin')

@section('title')
    Дни
@endsection

@section('header')
    Дни
@endsection

@section('content')
<div class="col-lg-3">
    <div class="form-group create-button">
        <a href="{{ route('days.create') }}" class="btn btn-outline-primary">Создать запись</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Дата</th>
            {{-- <th>Текст</th> --}}
            <th>Операция</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($days as $day)
                <tr>
                    <td>{{ $day->date }}</td>
                    {{-- <td>{{ str_limit($participate_note->content_ru, 120) }}</td> --}}
                    <td>
                        {{ Form::open(['route' => ['days.destroy', $day], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('days.edit', $day) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
