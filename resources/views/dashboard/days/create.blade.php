@extends('layouts.admin')

@section('title')
    Добавить запись
@endsection

@section('header')
    Добавить запись
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{ Form::open(['route' => 'days.index']) }}

    <ul class="nav nav-tabs" role="tablist">
        {{-- <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#ru" role="tab">Русский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#en" role="tab">Английский язык</a>
        </li> --}}
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#meta" role="tab">Мета-даные</a>
        </li>
    </ul>

    <div class="tab-content pt-3">
        <!-- Русская версия -->
        {{-- <div class="tab-pane fade show active" id="ru" role="tabpanel">
            
        </div> --}}
        <!-- Конец русской версии -->

        <!-- Английская версия -->
        {{-- <div class="tab-pane fade" id="en" role="tabpanel">

        </div> --}}
        <!-- Конец английской версии -->

        <!-- Мета-данные -->
        <div class="tab-pane fade show active" id="meta" role="tabpanel">
            <div class="form-group">
                {{ Form::label('date', 'Дата') }}
                {{ Form::date('date', null, ['class' => 'form-control' . ($errors->has('title_ru') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('date'))
                    @foreach ($errors->get('date') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец мета-данных -->
    </div>

    {{ Form::submit('Добавить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}

@endsection