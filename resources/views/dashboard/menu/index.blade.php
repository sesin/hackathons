@extends('layouts.admin')

@section('title')
    Меню
@endsection

@section('header')
    Меню
@endsection

@section('content')
<div class="col-lg-3">
    <div class="form-group create-button">
        <a href="{{ route('menu.create') }}" class="btn btn-outline-primary">Добавить пункт меню</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Текст</th>
            {{-- <th>Текст</th> --}}
            <th>Операция</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($menu_items as $menu_item)
                <tr>
                    <td>{{ $menu_item->text }}</td>
                    {{-- <td>{{ str_limit($participate_note->content_ru, 120) }}</td> --}}
                    <td>
                        {{ Form::open(['route' => ['menu.destroy', $menu_item], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('menu.edit', $menu_item) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
