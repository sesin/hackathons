@extends('layouts.admin-without-sidebar')

@section('content')
        <div class="card w-50 m-auto mt-5">
            <div class="card-body">
                <h3 class="card-title">Confirm delete</h3>
                <p class="card-text">
                    После того как вы нажмете "Удалить" запись "{{ $participate_note->name_ru }}" будет удалена,
                    и вы не сможете восстановить её в будущем.
                </p>
                {{ Form::open(['route' => ['participate-notes.destroy', $participate_note], 'method' => 'delete', 'style' => 'display: inline-block']) }}
                    {{ Form::hidden('confirmed', true) }}
                    <button class="btn btn-danger">Удалить</button>
                {{ Form::close() }}
                <a class="btn btn-secondary" href="{{ route('participate-notes.index') }}">Отмена</a>
            </div>
        </div>
@endsection
