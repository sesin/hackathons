@extends('layouts.admin')

@section('title')
    Добавить запись
@endsection

@section('header')
    Добавить запись
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{ Form::open(['route' => 'participate-notes.index']) }}

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Русский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Английский язык</a>
        </li>
        {{-- <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#contact" role="tab">Мета-даные</a>
        </li> --}}
    </ul>

    <div class="tab-content pt-3">
        <!-- Русская версия -->
        <div class="tab-pane fade show active" id="home" role="tabpanel">
            <div class="form-group">
                {{ Form::label('title_ru', 'Заголовок рус.') }}
                {{ Form::text('title_ru', null, ['class' => 'form-control' . ($errors->has('title_ru') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('title_ru'))
                    @foreach ($errors->get('title_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('content_ru', 'Текст.рус') }}
                {{ Form::textarea('content_ru', null, ['class' => 'form-control tinymce-val' . ($errors->has('content_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('content_ru'))
                    @foreach ($errors->get('content_ru') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец русской версии -->

        <!-- Английская версия -->
        <div class="tab-pane fade" id="profile" role="tabpanel">
            <div class="form-group">
                {{ Form::label('title_en', 'Заголовок анг.') }}
                {{ Form::text('title_en', null, ['class' => 'form-control' . ($errors->has('title_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('title_en'))
                    @foreach ($errors->get('title_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('content_en', 'Текст.анг') }}
                {{ Form::textarea('content_en', null, ['class' => 'form-control tinymce-val' . ($errors->has('content_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('content_en'))
                    @foreach ($errors->get('content_en') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец английской версии -->

        <!-- Мета-данные -->
        {{-- <div class="tab-pane fade" id="contact" role="tabpanel">
            
        </div> --}}
        <!-- Конец мета-данных -->
    </div>

    {{ Form::submit('Добавить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}

@endsection