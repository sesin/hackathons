@extends('layouts.admin')

@section('title')
    Редактирование новости {{ $participate_note->name_ru }}
@endsection

@section('header')
    Редактирование новости {{ $participate_note->name_ru }}
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{{ Form::model($participate_note, ['route' => ['participate-notes.update', $participate_note->id], 'method' => 'PUT']) }}

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Русский язык</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Английский язык</a>
        </li>
        {{-- <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#contact" role="tab">Мета-даные</a>
        </li> --}}
    </ul>

    <div class="tab-content pt-3">
        <!-- Русская версия -->
        <div class="tab-pane fade show active" id="home" role="tabpanel">
            <div class="form-group">
                {{ Form::label('title_ru', 'Заголовок рус.') }}
                {{ Form::text('title_ru', $participate_note->translate('ru')->title, ['class' => 'form-control' . ($errors->has('title_ru') ? ' is-invalid' : '')]) }}
        
                @if($errors->has('title_ru'))
                    @foreach ($errors->get('title_ru') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('content_ru', 'Текст.рус') }}
                {{ Form::textarea('content_ru', $participate_note->translate('ru')->content, ['class' => 'form-control tinymce-val' . ($errors->has('content_ru') ? ' is-invalid' : '')]) }}

                @if($errors->has('content_ru'))
                    @foreach ($errors->get('content_ru') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- Конец русской версии -->

        <!-- Английская версия -->
        <div class="tab-pane fade" id="profile" role="tabpanel">
            <div class="form-group">
                {{ Form::label('title_en', 'Заголовок анг.') }}
                {{ Form::text('title_en', $participate_note->translate('en')->title, ['class' => 'form-control' . ($errors->has('title_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('title_en'))
                    @foreach ($errors->get('title_en') as $message)
                        <div class="invalid-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('content_en', 'Текст.анг') }}
                {{ Form::textarea('content_en', $participate_note->translate('en')->content, ['class' => 'form-control tinymce-val' . ($errors->has('content_en') ? ' is-invalid' : '')]) }}

                @if($errors->has('content_en'))
                    @foreach ($errors->get('content_en') as $message)
                        <div class="form-control-feedback">{{ $message }}</div>
                    @endforeach
                @endif
            </div>
        </div>

        {{ Form::submit('Обновить', ['class' => 'btn btn-outline-primary']) }}

{{ Form::close() }}


@endsection