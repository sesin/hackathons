@extends('layouts.admin')

@section('title')
    Что ты получишь?
@endsection

@section('header')
    Что ты получишь?
@endsection

@section('content')
<div class="col-lg-3">
    <div class="form-group create-button">
        <a href="{{ route('participate-notes.create') }}" class="btn btn-outline-primary">Создать запись</a>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Заголовок</th>
            {{-- <th>Текст</th> --}}
            <th>Операция</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($participate_notes as $participate_note)
                <tr>
                    <td>{{ $participate_note->title }}</td>
                    {{-- <td>{{ str_limit($participate_note->content_ru, 120) }}</td> --}}
                    <td>
                        {{ Form::open(['route' => ['participate-notes.destroy', $participate_note], 'method' => 'delete']) }}
                            <div class="btn-group btn-group-sm" role="group">
                                <a href="{{ route('participate-notes.edit', $participate_note) }}" class="btn btn-light">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>

                                <button class="btn btn-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        {{ Form::close() }}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection
