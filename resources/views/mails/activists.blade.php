<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>First Letter</title>
    <style type="text/css">
    h1, h2{
        text-align: center;
    }
    table{
        width: 100%;
    }
    table td{
        border: 1px solid #ccc;
    }
    </style>
</head>
<body>
    <h1>Application Form for Civil Activists</h1>
    <h2>Call for Applications under</h2>
    <h2>2018 EaP Civil Society Hackathon</h2>
    <h2>Minsk, Belarus</h2>
 
    <h3>1. Personal information</h3>
    <table>
        <tr>
            <td></td>
            <td>Title:</td>
            <td>{{ $data['formData']['Title'] }}</td>
        </tr>
        <tr>
            <td>1.1</td>
            <td>First Name:</td>
            <td>{{ $data['formData']['firstName'] }}</td>
        </tr>
        <tr>
            <td>1.2</td>
            <td>Last Name:</td>
            <td>{{ $data['formData']['lastName'] }}</td>
        </tr>
        <tr>
            <td>1.3</td>
            <td>Contact phone No:</td>
            <td>{{ $data['formData']['Phone'] }}</td>
        </tr>
        <tr>
            <td>1.4</td>
            <td>Email Address:</td>
            <td>{{ $data['formData']['Email'] }}</td>
        </tr>
        <tr>
            <td>1.5</td>
            <td>Residential Address:</td>
            <td>
                <table>
                    <tr>
                        <td>Street, house, apartment:</td>
                        <td>{{ $data['formData']['streetHouseApartment'] }}</td>
                    </tr>
                    <tr>
                        <td>City/town:</td>
                        <td>{{ $data['formData']['City'] }}</td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td>{{ $data['formData']['residentialCountry'] }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>1.6</td>
            <td>Country of Citizenship:</td>
            <td>{{ $data['formData']['citizenshipCountry'] }}</td>
        </tr>
        <tr>
            <td>1.7</td>
            <td>Gender:</td>
            <td>{{ $data['formData']['Gender'] }}</td>
        </tr>
        <tr>
            <td>1.8</td>
            <td>Language skills:</td>
            <td>
                <table>
                    <tr>
                        <td></td>
                        <td>Reading</td>
                        <td>Speaking</td>
                        <td>Listening comprehension</td>
                    </tr>
                    <tr>
                        <td>English:</td>
                        <td>{{ $data['formData']['readingEn'] }}</td>
                        <td>{{ $data['formData']['speakingEn'] }}</td>
                        <td>{{ $data['formData']['listeningEn'] }}</td>
                    </tr>
                    <tr>
                        <td>Russian:</td>
                        <td>{{ $data['formData']['readingRu'] }}</td>
                        <td>{{ $data['formData']['speakingRu'] }}</td>
                        <td>{{ $data['formData']['listeningRu'] }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <h3>2. ICT tool Description (Please note that characters limit in this section is 1500 per question)</h3>
    <table>
        <tr>
            <td>2.1</td>
            <td>Title / name of the proposed ICT tool<br/>
                {{ $data['formData']['ictToolName'] }}
            </td>
        </tr>
        <tr>
            <td>2.2</td>
            <td>Targeted town(s)/province(s) and country(ies)<br/>
                {{ $data['formData']['targetTowns'] }}
            </td>
        </tr>
        <tr>
            <td>2.3</td>
            <td>Describe your ICT tool in brief (not more than 100 words)<br/>
                {{ $data['formData']['toolDescription'] }}
            </td>
        </tr>
        <tr>
            <td>2.4</td>
            <td>The problem to be addressed and how your ICT tool will help to solve it<br/>
                {{ $data['formData']['Problem'] }}
            </td>
        </tr>
        <tr>
            <td>2.5</td>
            <td>Describe the technical implementation of your ICT tool<br/>
                {{ $data['formData']['technicalImplementation'] }}
            </td>
        </tr>
        <tr>
            <td>2.6</td>
            <td>Describe how your ICT tool be maintained and updated in future<br/>
                {{ $data['formData']['toolFuture'] }}
            </td>
        </tr>
        <tr>
            <td>2.7</td>
            <td>Are there any other ICT tools that tackle the same issue in your home country, other EaP countries, in Europe or in the world?<br/>
                {{ $data['formData']['otherTool'] }}
            </td>
        </tr>
        <tr>
            <td>2.8</td>
            <td>
                <table>
                    <tr>
                        <td>In your opinion, what areas of e-development of civil society and citizens does your ICT tool contribute to? Please select not more than three areas</td>
                        <td>{{ implode(",", $data['formData']['areasOfEdevelopment']) }}
                            @if(isset( $data['formData']['developmentAreasTextarea'] ))
                                <div>{{ $data['formData']['developmentAreasTextarea'] }}</div>
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <h3>3. Relevant experience: (Please note that characters limit in this section is 1000 per question)</h3>
    <table>
        <tr>
            <td>3.1</td>
            <td>Do you have experience of working or volunteering in civil society organisation, community or civil movement, in any form?</td>
            <td>{{ $data['formData']['workingExperience'] }}</td>
        </tr>
        <tr>
            <td>3.2</td>
            <td>Are you currently working or volunteering in CSO, community or civil movement?</td>
            <td>{{ $data['formData']['currentlyWorking'] }}
                @if(isset( $data['formData']['currentlyWorkingTextarea'] ))
                    <div>{{ $data['formData']['currentlyWorkingTextarea'] }}</div>
                @endif
            </td>
        </tr>
        <tr>
            <td>3.3</td>
            <td>Provide the name and links (website or social media pages) of the organisation(s) you are associated with (if any):</td>
            <td>{{ $data['formData']['organisationsAssosicatedWith'] }}</td>
        </tr>
        <tr>
            <td>3.4</td>
            <td>Describe your position and how long you have been working with the organisation(s)</td>
            <td>{{ $data['formData']['positionsAndTermsOfWorking'] }}</td>
        </tr>
        <tr>
            <td>3.5</td>
            <td>Are you a Founder of the organisation?</td>
            <td>{{ $data['formData']['founderOfOrganisation'] }}</td>
        </tr>
        <tr>
            <td>3.6</td>
            <td>Is the Board or management of your organisation/community aware of your interest in this Hackathon programme?</td>
            <td>{{ $data['formData']['organisationAwareOfInterest'] }}</td>
        </tr>
        <tr>
            <td>3.7</td>
            <td>Have you ever been participated in or lead a project on design and development of any IT products? If yes, can you describe the project and results achieved?</td>
            <td>{{ $data['formData']['participateItProducts'] }}</td>
        </tr>
    </table>

    <h3>4. Hack team</h3>
    <table>
        <tr>
            <td>4.1</td>
            <td>Do you have a hack team to work with you at 2018 EaP Civil Society Hackathon?</td>
            <td>{{ $data['formData']['hackTeamWorkWith'] }}
                @if($data['formData']['hackTeamWorkWith'] == 'Yes')
                    <div>
                        Hack Team Member 1:
                        <table>
                            <tr>
                                <td>First and Last Name:</td>
                                <td>{{ $data['formData']['firstHackMemberName'] }}</td>
                            </tr>
                            <tr>
                                <td>Citizenship:</td>
                                <td>{{ $data['formData']['firstHackMemberCitizenshipCountry'] }}</td>
                            </tr>
                            <tr>
                                <td>Street, house, apartment:</td>
                                <td>{{ $data['formData']['firstHackMemberStreet'] }}</td>
                            </tr>
                            <tr>
                                <td>City/town:</td>
                                <td>{{ $data['formData']['firstHackMemberCity'] }}</td>
                            </tr>
                            <tr>
                                <td>Country:</td>
                                <td>{{ $data['formData']['firstHackMemberResidetialCountry'] }}</td>
                            </tr>
                            <tr>
                                <td>Technical expertise</td>
                                <td>{{ $data['formData']['firstHackMemberExpertise'] }}</td>
                            </tr>
                            <tr>
                                <td>Role in the hack team</td>
                                <td>{{ $data['formData']['firstHackMemberRole'] }}</td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
                        Hack Team Member 2 (optional):
                        <table>
                            <tr>
                                <td>First and Last Name:</td>
                                <td>{{ $data['formData']['secondHackMemberName'] }}</td>
                            <tr>
                            </tr>
                                <td>Citizenship:</td>
                                <td>{{ $data['formData']['secondHackMemberCitizenshipCountry'] }}</td>
                            <tr>
                            </tr>
                                <td>Street, house, apartment:</td>
                                <td>{{ $data['formData']['secondHackMemberStreet'] }}</td>
                            <tr>
                            </tr>
                                <td>City/town:</td>
                                <td>{{ $data['formData']['secondHackMemberCity'] }}</td>
                            <tr>
                            </tr>
                                <td>Country:</td>
                                <td>{{ $data['formData']['secondHackMemberResidetialCountry'] }}</td>
                            <tr>
                            </tr>
                                <td>Technical expertise</td>
                                <td>{{ $data['formData']['secondHackMemberExpertise'] }}</td>
                            <tr>
                            </tr>
                                <td>Role in the hack team</td>
                                <td>{{ $data['formData']['secondHackMemberRole'] }}</td>
                            </tr>
                        </table>
                    </div>
                @else
                    <div>{{ implode(",", $data['formData']['technicalExperts']) }}
                        @if(isset( $data['formData']['technicalExpertsTextarea'] ))
                            <div>{{ $data['formData']['technicalExpertsTextarea'] }}</div>
                        @endif
                    </div>
                @endif
            </td>
        </tr>
    </table>

    <h3>5. Additional information (Please note that this section characters limit is 1000 per question)</h3>
    <table>
        <tr>
            <td>5.1</td>
            <td>Any further personal information pertinent to this Hackathon:</td>
            <td>{{ $data['formData']['personalInformation'] }}</td>
        </tr>
    </table>

    @if( isset($data['url']) )
        <h3>Attachments</h3>
        <a href="{{$data['url']}}">Link to attachment</a>
    @endif
</body>
</html>