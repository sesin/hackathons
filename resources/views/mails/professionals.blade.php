<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>First Letter</title>
    <style type="text/css">
    h1, h2{
        text-align: center;
    }
    table{
        width: 100%;
    }
    table td{
        border: 1px solid #ccc;
    }
    </style>
</head>
<body>
    <h1>Application Form for IT professionals and Graphic Designers</h1>
    <h2>Call for Applications under</h2>
    <h2>2018 EaP Civil Society Hackathon</h2>
    <h2>Minsk, Belarus</h2>

    <h3>1. Personal information</h3>
    <table>
        <tr>
            <td></td>
            <td>Title:</td>
            <td>{{ $data['formData']['Title'] }}</td>
        </tr>
        <tr>
            <td>1.1</td>
            <td>First Name:</td>
            <td>{{ $data['formData']['firstName'] }}</td>
        </tr>
        <tr>
            <td>1.2</td>
            <td>Last Name:</td>
            <td>{{ $data['formData']['lastName'] }}</td>
        </tr>
        <tr>
            <td>1.3</td>
            <td>Contact phone No:</td>
            <td>{{ $data['formData']['Phone'] }}</td>
        </tr>
        <tr>
            <td>1.4</td>
            <td>Email Address:</td>
            <td>{{ $data['formData']['Email'] }}</td>
        </tr>
        <tr>
            <td>1.5</td>
            <td>Residential Address:</td>
            <td>
                <table>
                    <tr>
                        <td>Street, house, apartment:</td>
                        <td>{{ $data['formData']['streetHouseApartment'] }}</td>
                    </tr>
                    <tr>
                        <td>City/town:</td>
                        <td>{{ $data['formData']['City'] }}</td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td>{{ $data['formData']['residentialCountry'] }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>1.6</td>
            <td>Country of Citizenship:</td>
            <td>{{ $data['formData']['citizenshipCountry'] }}</td>
        </tr>
        <tr>
            <td>1.7</td>
            <td>Gender:</td>
            <td>{{ $data['formData']['Gender'] }}</td>
        </tr>
        <tr>
            <td>1.8</td>
            <td>Language skills:</td>
            <td>
                <table>
                    <tr>
                        <td></td>
                        <td>Reading</td>
                        <td>Speaking</td>
                        <td>Listening comprehension</td>
                    </tr>
                    <tr>
                        <td>English:</td>
                        <td>{{ $data['formData']['readingEn'] }}</td>
                        <td>{{ $data['formData']['speakingEn'] }}</td>
                        <td>{{ $data['formData']['listeningEn'] }}</td>
                    </tr>
                    <tr>
                        <td>Russian:</td>
                        <td>{{ $data['formData']['readingRu'] }}</td>
                        <td>{{ $data['formData']['speakingRu'] }}</td>
                        <td>{{ $data['formData']['listeningRu'] }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <h3>2. Relevant experience: (Please note that characters limit in this section is 1000 per question)</h3>
    <table>
        <tr>
            <td>2.1</td>
            <td>Select your main area of expertise</td>
            <td>{{ $data['formData']['Expertise'] }}</td>
        </tr>
        <tr>
            <td>2.2</td>
            <td>Employment Status:</td>
            <td>{{ $data['formData']['employementStatus'] }}
                @if(isset( $data['formData']['otherEmployement'] ))
                    <div>{{ $data['formData']['otherEmployement'] }}</div>
                @endif
            </td>
        </tr>
        <tr>
            <td>2.3</td>
            <td>How many years (cumulative) have you been working in IT industry or as a designer?</td>
            <td>{{ $data['formData']['workExperience'] }}</td>
        </tr>
        <tr>
            <td>2.4</td>
            <td>Describe your areas of expertise and professional experience in IT and/or graphic design. Please provide links to successful projects, if available</td>
            <td>{{ $data['formData']['linksToProject'] }}</td>
        </tr>
        <tr>
            <td>2.5</td>
            <td>Please provide a link(s) to your website or portfolio</td>
            <td>{{ $data['formData']['linkToPortfolio'] }}</td>
        </tr>
    </table>

    <h3>4. Motivation (Please note that characters limit in this section is 1000 per question)</h3>
    <table>
        <tr>
            <td>4.1</td>
            <td>Do you have previous experience of working with civil society organisations, local communities or groups of activists?</td>
            <td>{{ $data['formData']['previousExperience'] }}
                @if(isset( $data['formData']['previousExperienceTextarea'] ))
                    <div>{{ $data['formData']['previousExperienceTextarea'] }}</div>
                @endif
            </td>
        </tr>
        <tr>
            <td>4.2</td>
            <td>Explain in brief why do you want to participate in 2018 EaP Civil Society Hackathon?</td>
            <td>{{ $data['formData']['whyParticipateBrief'] }}</td>
        </tr>
    </table>

    <h3>5. Additional information (Please note that this section characters limit is 1000 per question)</h3>
    <table>
        <tr>
            <td>5.1</td>
            <td>Any further personal information pertinent to this Hackathon:</td>
            <td>{{ $data['formData']['personalInformation'] }}</td>
        </tr>
    </table>
</body>
</html>