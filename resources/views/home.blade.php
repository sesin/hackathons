@extends('layouts.app')

@section('content')

<section class="banner-section">
    <div class="img-wrap">
        <img src="/storage/{{ $settings->settings->banner_image_path }}" alt="">
    </div>
    <div class="bnr-text">
        <div class="container">
            <div class="inner">
                <h1 class="banner-title">{!! $settings->translated_settings->banner_title !!}</h1>
                <div class="bnr-descr">
                    {!! $settings->translated_settings->banner_text !!}
                </div>
                <a href="{{ $settings->settings->button_url }}" class="btn white-btn">{{ $settings->translated_settings->button_text }}</a>
            </div>
        </div>

    </div>
</section>

<section class="theme-section" id="about-project">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 about-block">
                <h4 class="title">{{ trans('main.about-the-hackathon') }}</h4>
                <div class="descr">
                    <div class="text_review">
                        {!! $settings->translated_settings->about_hackathon_announcement !!}
                    </div>
                    <br>
                    <br>
                    <span class="read-more" data-toggle="modal" data-target="#modal_about">{{ trans('main.read-more') }}</span>
                </div>
            </div>

            <div class="col-xl-4 counter-block">
                <div class="counter-wrap">
                    <div class="block-title" data-end-text="Hackathon has already started:">{{ trans('main.hackathon-starts-in') }}
                    </div>
                    <div class="start-counter" data-countdown="2018/7/1"></div>
                    <a href="/" class="btn white-btn">{{ trans('main.registration') }}</a>
                </div>
            </div>
            <div class="col-xl-4 news-block">
                <h4 class="title">{{ trans('main.news-and-updates') }}</h4>
                <div class="news-list scroll-pane">
                    @foreach($post_news as $news)
                        <div class="news-item" data-toggle="modal" data-target="#modal_news_{{ $loop->iteration }}">
                            <div class="news-title">
                                {!! $news->title !!}
                            </div>
                            <div class="date">{{ $news->created_at->formatLocalized('%d %b, %Y') }}</div>
                        </div>
                    @endforeach
                </div>
                @if($post_news->count() > 4)
                    <div class="show-more" id="more-news">{{ trans('main.more-news') }}</div>
                @endif
            </div>
        </div>
        <div class="social-block">
            <div class="social">
                <a href="#" class="share-icn"><i class="fa fa-share-alt"></i></a>
                <a data-network="facebook" href="#" class="fb st-custom-button">fb</a>
                <a data-network="twitter" href="#" class="tw st-custom-button">tw</a>
                <a data-network="linkedin" href="#" class="in st-custom-button">in</a>
            </div>
        </div>
    </div>
</section>

<section class="theme-section grey-bg" id="why-section">
    <div class="container">
        <h4 class="title">{{ trans('main.why-to-participate') }}</h4>
        <div class="section-content">
            <div class="row">
                @foreach($participate_notes as $participate_note)
                    <div class="col-md-4 why-item">
                        <div class="num">{{ $loop->iteration  }}.</div>
                        <div class="why-title">
                            {!! $participate_note->title !!}
                        </div>
                        <div class="text">
                            {!! $participate_note->content !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="theme-section" id="mentors-section">
    <div class="container">
        <h4 class="title">{{ trans('main.mentors') }}</h4>
        <div class="section-content">
            <div id="mentor-slider" class="mobile-slider">
                <div class="row">
                    @foreach($mentors as $mentor)
                        <div class="col-md-4 mentors-item">
                            <div class="mentor-wrap" data-toggle="modal" data-target="#modal_mentor_{{ $loop->iteration }}">
                                <div class="top-info">
                                    <div class="img-wrap">
                                        <img src="/storage/{{ $mentor->image_path }}" alt="">
                                    </div>
                                    <div class="soc-block">
                                        @if($mentor->facebook_url)
                                            <a href="{{ $mentor->facebook_url }}" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
                                        @endif
                                        @if($mentor->twitter_url)
                                            <a href="{{ $mentor->twitter_url }}" target="_blank" class="tw"><i class="fa fa-twitter"></i></a>
                                        @endif
                                        @if($mentor->linkedin_url)
                                            <a href="{{ $mentor->linkedin_url }}" target="_blank" class="in"><i class="fa fa-linkedin"></i></a>
                                        @endif
                                    </div>
                                </div>
                                <div class="bottom-info">
                                    <div class="name">{{ $mentor->name }}</div>
                                    <div class="mentor-status">{{ $mentor->profession }}</div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            {{-- <div id="mentor-thumbnails" class="slider-thumbnails">
                <div class="tramb-item">
                    @foreach($mentors as $mentor)
                        <img src="/storage/{{ $mentor->image_path }}" alt="">
                    @endforeach
                </div>
            </div> --}}
        </div>
    </div>
</section>

<section class="theme-section grey-bg" id="agenda-section">
    <div class="container">
        <h4 class="title">{{ trans('main.agenda') }}</h4>
        <div class="section-content">
            <div class="card-wrapper" id="agenda-accordion">
                <div class="card-header-wrap">
                    @foreach($days as $day)     
                        <div class="card-header" id="day{{ $loop->iteration }}">
                            <div class="card-header-box {{ $loop->iteration == 1 ? '' : 'collapsed' }}" data-toggle="collapse" data-target="#collapseDay{{ $loop->iteration }}"
                                 aria-expanded="{{ $loop->iteration == 1 ? 'true' : 'false' }}" aria-controls="collapseDay{{ $loop->iteration }}">
                                <div class="day-num">{{ trans('main.day') . " " . $loop->iteration }}</div>
                                <div class="date">{{ $day->date->formatLocalized('%A, %d %B %Y') }}</div>
                            </div>
                        </div>
                    @endforeach
                </div><!--/card-header-wrap-->

                <div class="card-body-wrap">
                    @foreach($days as $day)
                        <div id="collapseDay{{ $loop->iteration }}" class="collapse {{ $loop->iteration == 1 ? 'show' : '' }}" aria-labelledby="day{{ $loop->iteration }}" data-parent="#agenda-accordion">
                            <div class="card-body scroll-pane">
                                @foreach($day->lectures as $lecture)
                                    <div class="event-line">
                                        <div class="event-first-info">
                                            <div class="wrap">
                                                <div class="time">
                                                    <svg class="icon" id="i-clock" viewBox="0 0 32 32" width="16" height="16"
                                                         fill="none" stroke="currentcolor" stroke-linecap="round"
                                                         stroke-linejoin="round" stroke-width="2">
                                                        <circle cx="16" cy="16" r="14"/>
                                                        <path d="M16 8 L16 16 20 20"/>
                                                    </svg>
                                                    @if($lecture->start_at && $lecture->end_at)
                                                        <span>{{ $lecture->start_at }} – {{ $lecture->end_at }}</span>
                                                    @else
                                                        <span>{{ $lecture->start_at }}</span>
                                                    @endif
                                                </div>      

                                                @if($lecture->place)
                                                    <div class="place">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        <span>{{ $lecture->place }}</span>
                                                    </div>
                                                @endif

                                                @if($lecture->mentors)
                                                    @foreach($lecture->mentors as $mentor)
                                                        <div class="lecturer">
                                                            <i class="fa fa-microphone" aria-hidden="true"></i>
                                                            <span>{{ trans('main.lectors') }}</span>
                                                        </div>
                                                        <div class="avatar">
                                                            <img src="/thumbs/{{ $mentor->image_path }}" alt="">
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="event-last-info">
                                            <div class="event-title">{{ $lecture->title }}</div>
                                            <div class="event-deskr">
                                                {!! $lecture->content !!}
                                            </div>
                                        </div>
                                    </div><!--/event-line-->
                                @endforeach
                            </div>
                            <div class="show-more more-event">{{ trans('main.more') }}</div>
                        </div>
                    @endforeach
                </div><!--/card-body-wrap-->
            </div>
        </div>
    </div>
</section>

<section class="theme-section" id="was-section">
    <div class="container">
        <h4 class="title">{{ trans('main.how-it-was') }}</h4>
        <div class="section-content">
            <div id="was-slider" class="mobile-slider">
                <div class="row">
                    @foreach($albums as $album)
                        @if($loop->iteration == 1) <!-- Код для первого альбома -->

                            <div class="col-md-7 was-item">
                                @if($album->albumItems[0]->type == 'image') <!-- Если альбом из картинок -->
                                    <a href="{{$album->albumItems[0]->large_image }}" class="was-box" data-fancybox="quick-view-1" data-type="image">
                                        <div class="img-wrap"> 
                                            <img src="/storage/{{ $album->cover_image_path }}">
                                        </div>
                                        <div class="box-title">{{ $album->name }}</div>
                                    </a>
                                    <div class="fancy-box">
                                        @foreach($album->albumItems as $item)
                                            <a href="{{ $item->large_image }}" data-fancybox="quick-view-1" data-type="image"></a>
                                        @endforeach
                                    </div>
                                @elseif($album->albumItems[0]->type == 'url') <!-- Если альбом из видео -->
                                    <a class="was-box" data-fancybox href="{{ $album->albumItems[0]->url }}">
                                        <div class="img-wrap">
                                            <img src="/storage/{{ $album->cover_image_path }}" alt="">
                                        </div>
                                        <div class="box-title">{{ $album->name }}</div>
                                    </a>
                                @endif
                            </div>
                            
                            <div class="col-md-5 was-item">
                                <a class="was-box" href="http://ideas.eapcivilsociety.eu/" target="_blank">
                                    <div class="img-wrap">
                                        <img src="/storage/images/img_2.png" alt="">
                                    </div>
                                    <div class="box-title">2017 Hackathon Projects</div>
                                </a>
                            </div>
                        {{-- @elseif($loop->iteration == 2) <!-- Код для второго альбома -->
                            <div class="col-md-5 was-item">
                                @if($album->albumItems[0]->type == 'image')
                                    <a href="{{$album->albumItems[0]->large_image }}" class="was-box" data-fancybox="quick-view-2" data-type="image">
                                        <div class="img-wrap">
                                            <img src="/storage/{{ $album->cover_image_path }}">
                                        </div>
                                        <div class="box-title">{{ $album->name }}</div>
                                    </a>
                                    <div class="fancy-box">
                                        @foreach($album->albumItems as $item)
                                            <a href="{{ $item->large_image }}" data-fancybox="quick-view-2" data-type="image"></a>
                                        @endforeach
                                    </div>
                                @elseif($album->albumItems[0]->type == 'url')
                                    <a class="was-box" data-fancybox href="{{ $album->albumItems[0]->url }}">
                                        <div class="img-wrap">
                                            <img src="/storage/{{ $album->cover_image_path }}" alt="">
                                        </div>
                                        <div class="box-title">{{ $album->name }}</div>
                                    </a>
                                @endif
                            </div> --}}
                        @else <!-- Код для всех остальных альбомов -->
                            <div class="col-md-4 was-item">
                                @if($album->albumItems[0]->type == 'image')
                                    <a href="/storage/{{ $album->cover_image_path }}" class="was-box" data-fancybox="quick-view-{{ $loop->iteration }}" data-type="image">
                                        <div class="img-wrap">
                                            <img src="/storage/{{ $album->cover_image_path }}">
                                        </div>
                                        <div class="box-title">{{ $album->name }}</div>
                                    </a>
                                    <div class="fancy-box">
                                        @foreach($album->albumItems as $item)
                                            <a href="{{ $item->large_image }}" data-fancybox="quick-view-{{ $loop->parent->iteration }}" data-type="image"></a>
                                        @endforeach
                                    </div>
                                @elseif($album->albumItems[0]->type == 'url')
                                    <a class="was-box" data-fancybox href="{{ $album->albumItems[0]->url }}">
                                        <div class="img-wrap">
                                            <img src="/storage/{{ $album->cover_image_path }}" alt="">
                                        </div>
                                        <div class="box-title">{{ $album->name }}</div>
                                    </a>
                                @endif
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            {{-- <div id="was-thumbnails" class="slider-thumbnails">
                @foreach($albums as $album)
                    <div class="tramb-item">
                        <img src="/storage/{{ $album->cover_image_path }}" alt="">
                    </div>
                @endforeach
            </div> --}}
        </div>
    </div>
</section>

<section class="theme-section grey-bg" id="rules-section">
    <div class="container">
        <h4 class="title">{{ trans('main.rules-and-faq') }}</h4>
        <div class="section-content">
            <div class="card-wrapper simple-accordion" id="rules-accordion">
                <div class="row">
                    <div class="col-md-4 rules-item">
                        <div class="card-header-wrap">
                            @foreach($rules as $rule)
                                <div class="card-header" id="rules{{ $loop->iteration }}">
                                    <div class="card-header-box {{ $loop->iteration == 1 ? '' : 'collapsed' }}" data-toggle="collapse" data-target="#collapseRule{{ $loop->iteration }}"
                                         aria-expanded="{{ $loop->iteration == 1 ? 'true' : 'false' }}" aria-controls="collapseRule{{ $loop->iteration }}">
                                        {{ $rule->title }}
                                    </div>
                                </div>
                            @endforeach
                        </div><!--/card-header-wrap-->

                        <div class="btn-wrap">
                            <a href="/storage/{{ $settings->translated_settings->rules_file_path }}" class="btn btn-mini blue-btn download-btn">{{ trans('main.download-guidelines') }}</a>
                        </div>
                    </div>

                    <div class="col-md-8 rules-item">
                        <div class="card-body-wrap">
                            @foreach($rules as $rule)
                                <div id="collapseRule{{ $loop->iteration }}" class="collapse {{ $loop->iteration == 1 ? 'show' : '' }}" aria-labelledby="rules{{ $loop->iteration }}"
                                     data-parent="#rules-accordion">
                                    <div class="card-body scroll-pane">
                                        {!! $rule->content !!}
                                    </div>
                                </div>
                            @endforeach
                        </div><!--/card-body-wrap-->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="theme-section" id="partners-section">
    <div class="container">
        <div class="title-block">
            <h4 class="title">{{ trans('main.partners') }}</h4>
            <a href="http://ideas.eapcivilsociety.eu/" target="_blank" class="main-partner">
                Idea bank <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </a>
        </div>

        <div class="section-content">
            <div class="partners-carusel">
                @foreach($partners as $partner)
                    <div class="partner-item">
                        <a class="inner" href="{{ $partner->link }}" target="_blank">
                            <div class="img-wrap">
                                <img src="/storage/{{ $partner->logo }}" alt="">
                            </div>
                            <div class="partner-name">{{ $partner->caption }}</div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="theme-section grey-bg" id="contacts-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 contacts-line">
                <h4 class="title">{{ trans('main.contacts') }}</h4>
                <div class="contact-box">
                    {!! $contacts->translated_contacts->main_text !!}
                </div>
                <div class="btn-wrap">
                    <a href="{{ $contacts->contacts->button_url }}" class="btn red-btn">{{ $contacts->translated_contacts->button_text }}</a>
                </div>
            </div>
            <div class="col-md-8 map-line">
                <div class="map">
                    <iframe src="https://www.google.com/maps/d/embed?mid=1OnXAhMV7ciKpav5aVgTEf8GdmQ3M6GxT&hl={{ App::getLocale() }}" width="100%" height="100%"></iframe>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Modals -->

<!-- ABOUT Modals-->
<div class="modal fade modal_news" id="modal_about" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <svg id="i-close" viewBox="0 0 32 32" width="18" height="18" fill="none" stroke="currentcolor"
                     stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                    <path d="M2 30 L30 2 M30 30 L2 2"/>
                </svg>
            </button>

            <div class="top-info">
                <div class="title-wrap">
                    <h5 class="title-block">{{ trans('main.about-the-hackathon') }}</h5>
                    {{-- <div class="social">
                        <a href="#" class="share-icn"><i class="fa fa-share-alt"></i></a>
                        <a href="#" class="fb">fb</a>
                        <a href="#" class="tw">tw</a>
                        <a href="#" class="in">in</a>
                    </div> --}}
                </div>
            </div>
            <div class="content-box scroll-pane">
                {!! $settings->translated_settings->about_hackathon_text !!}
            </div>
        </div>
    </div>
</div>

<!-- NEWS Modals-->
<!-- NEWS 1-->
@foreach($post_news as $news)
    <div class="modal fade modal_news" id="modal_news_{{$loop->iteration}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg id="i-close" viewBox="0 0 32 32" width="18" height="18" fill="none" stroke="currentcolor"
                         stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                        <path d="M2 30 L30 2 M30 30 L2 2"/>
                    </svg>
                </button>

                <div class="top-info">
                    <div class="date-wrap">
                        <div class="day">{{ $news->created_at->formatLocalized('%d') }}</div>
                        <div class="year">{{ $news->created_at->formatLocalized('%B, %Y') }}</div>
                    </div>
                    <div class="title-wrap">
                        <h5 class="title-block">{{ $news->title }}</h5>
                        {{-- <div class="social">
                            <a href="#" class="share-icn"><i class="fa fa-share-alt"></i></a>
                            <a href="#" class="fb">fb</a>
                            <a href="#" class="tw">tw</a>
                            <a href="#" class="in">in</a>
                        </div> --}}
                    </div>
                </div>
                <div class="content-box scroll-pane">
                    {!! $news->content !!}
                </div>
            </div>
        </div>
    </div>
@endforeach


<!-- MENTORS Modals-->
@foreach($mentors as $mentor)
    <div class="modal fade modal_mentor" id="modal_mentor_{{ $loop->iteration }}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg id="i-close" viewBox="0 0 32 32" width="18" height="18" fill="none" stroke="currentcolor"
                         stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                        <path d="M2 30 L30 2 M30 30 L2 2"/>
                    </svg>
                </button>
                <div class="row">
                    <div class="col-md-6 modal-skills-info">
                        <div class="top-info">
                            <div class="img-wrap">
                                <img src="/storage/{{ $mentor->image_path }}" alt="">
                            </div>
                            <div class="soc-block">
                                @if($mentor->facebook_url)
                                    <a href="{{ $mentor->facebook_url }}" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
                                @endif
                                @if($mentor->twitter_url)
                                    <a href="{{ $mentor->twitter_url }}" target="_blank" class="tw"><i class="fa fa-twitter"></i></a>
                                @endif
                                @if($mentor->linkedin_url)
                                    <a href="{{ $mentor->linkedin_url }}" target="_blank" class="in"><i class="fa fa-linkedin"></i></a>
                                @endif
                            </div>
                        </div>

                        <div class="skills-wrap">
                            @if($mentor->skill_one)
                                <div class="skills-item">
                                    <div class="skills-title">
                                        <span class="name">{{ $mentor->skill_one }}</span>
                                        {{-- <span class="rate">{{ $mentor->skill_one_percentage }}%</span> --}}
                                    </div>
                                    <div class="skills-line">
                                        <div class="current" data-rate="{{ $mentor->skill_one_percentage }}"></div>
                                    </div>
                                </div>
                            @endif
                            @if($mentor->skill_two)
                                <div class="skills-item">
                                    <div class="skills-title">
                                        <span class="name">{{ $mentor->skill_two }}</span>
                                        {{-- <span class="rate">{{ $mentor->skill_two_percentage }}%</span> --}}
                                    </div>
                                    <div class="skills-line">
                                        <div class="current" data-rate="{{ $mentor->skill_two_percentage }}"></div>
                                    </div>
                                </div>
                            @endif
                            @if($mentor->skill_three)
                                <div class="skills-item">
                                    <div class="skills-title">
                                        <span class="name">{{ $mentor->skill_three }}</span>
                                        {{-- <span class="rate">{{ $mentor->skill_three_percentage }}%</span> --}}
                                    </div>
                                    <div class="skills-line">
                                        <div class="current" data-rate="{{ $mentor->skill_three_percentage }}"></div>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                    <div class="col-md-6 modal-text-info">
                        <div class="text-header">
                            <div class="name">{{ $mentor->name }}</div>
                            <div class="mentor-status">{{ $mentor->profession }}</div>
                        </div>
                        <div class="text-wrap scroll-pane">
                            {!! $mentor->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach

@endsection
