
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./jquery.mCustomScrollbar.concat.min.js');
require('./jquery.countdown.min.js');
require('slick-carousel');

require('@fancyapps/fancybox');


require('./common');
require('./form');