$(document).ready(function () {

    /*
     ** Sticky header
     */

    var stickyNavTop = $('#header').height();

    var stickyNav = function () {
        var scrollTop = $(window).scrollTop();

        if (scrollTop > stickyNavTop) {
            $('#header').addClass('stick');
        } else {
            $('#header').removeClass('stick');
        }
    };

    stickyNav();

    $(window).scroll(function () {
        stickyNav();
    });

    /*
     ** Адаптивная высота
     */
    function adaptiveHeigh(elem, param) {
        var windowHeight = $(window).height();
        $(elem).css(param, windowHeight);

        $(window).resize(function () {
            adaptiveHeigh(elem, param);
        });
    }

    adaptiveHeigh('.banner-section .img-wrap','height');

    adaptiveHeigh('body.no-front .page_content','min-height');



    /*
     ** Обрезаем текст
     */
    var showChar = 920;  // How many characters are shown by default
    var ellipsestext = "...";


    $('.text_review').each(function () {
        var content = $(this).html();
        //console.log(content.length);

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext + '</span>';

            $(this).html(html);
        }

    });

    /*
     ** Counter
     */

    $('[data-countdown]').each(function () {
        var $this = $(this),
            finalDate = $(this).data('countdown'),
            finishText = '';
        $this.countdown(finalDate, {elapse: true}).on('update.countdown', function (event) {
            var $this = $(this).html(event.strftime(''
                + '<div class="count-item"><span class="count-num">%D</span><span class="count-text">days</span></div>'
                + '<div class="count-item"><span class="count-num">%H</span><span class="count-text">hours</span></div>'
                + '<div class="count-item"><span class="count-num">%M</span><span class="count-text">minutes</span></div>'
                + '<div class="count-item"><span class="count-num">%S</span><span class="count-text">seconds</span></div>'));

            if (event.elapsed) {
                finishText = $this.parent().find('.block-title').data('end-text');
                $this.parent().find('.block-title').text(finishText);
            }
        });
    });

    /*
     ** Scroll
     */

    function checkSize(){
        if ($(window).width() >= '768') {
            $(".scroll-pane").mCustomScrollbar({
                theme: "dark"
            });
        } else {
            $(".modal-content .scroll-pane, #rules-section .scroll-pane").mCustomScrollbar({
                theme: "dark"
            });

            moreButton('#more-news', '.news-list .news-item', 3);
            eventAccordionMobile();


            //slider_mobile
            mobileSlider('#mentor-slider .row', '#mentor-thumbnails');
            mobileSlider('#was-slider .row', '#was-thumbnails');


        }

        function mobileSlider(sliderId, naviId) {
            $(sliderId).not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                //asNavFor: naviId,
                prevArrow: '<button type="button" class="slick-prev">' +
                '<svg id="i-arrow-left" viewBox="0 0 32 32" width="18" height="18" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">\n' +
                '<path d="M10 6 L2 16 10 26 M2 16 L30 16" />\n' +
                '</svg></button>',
                nextArrow: '<button type="button" class="slick-next">' +
                '<svg id="i-arrow-right" viewBox="0 0 32 32" width="18" height="18" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">\n' +
                '<path d="M22 6 L30 16 22 26 M30 16 L2 16" />\n' +
                '</svg></button>'
            });

            $(naviId).not('.slick-initialized').slick({
                slidesToShow: 7,
                slidesToScroll: 1,
                asNavFor: sliderId,
                dots: false,
                focusOnSelect: true,
                prevArrow: '<button type="button" class="slick-prev">' +
                '<svg id="i-arrow-left" viewBox="0 0 32 32" width="14" height="14" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">\n' +
                '<path d="M10 6 L2 16 10 26 M2 16 L30 16" />\n' +
                '</svg></button>',
                nextArrow: '<button type="button" class="slick-next">' +
                '<svg id="i-arrow-right" viewBox="0 0 32 32" width="14" height="14" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">\n' +
                '<path d="M22 6 L30 16 22 26 M30 16 L2 16" />\n' +
                '</svg></button>',
                responsive: [
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 318,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }
    }

    checkSize();

    // run test on resize of the window
    $(window).resize(checkSize, eventAccordionMobile);

    if($('.step-slide-line').length>0){
        var allSteps = $('.step-slide-line').data('all-steps');
        var currentStep = $('.step-slide-line').data('step');
        var slideWidth = 100 / allSteps * currentStep;
        $('.step-slide-line .current').width(slideWidth + '%');
    }



    function moreButton(button, element, num) {
        var buttonText = $(button).text();
        var element = element;
        var curElement = element + ':gt(' + num + ')';
        $(element).addClass('show-box');
        $(curElement).addClass('hide-box').removeClass('show-box');
        $(button).click(function () {
            //$('.news-list .news-item:gt(2)').addClass('hide-box');
            if ($(curElement).hasClass('hide-box')) {
                $(curElement).addClass('show-box').removeClass('hide-box');
                $(this).text('hide');
            } else {
                $(curElement).addClass('hide-box').removeClass('show-box');
                $(this).text(buttonText);
            }


        });
    }

    function eventAccordionMobile() {

        $('#agenda-accordion').find('.show .show-more').text('more');
        $('#agenda-accordion').find('.show .card-body .event-line').addClass('show-box');
        $('#agenda-accordion').find('.show .card-body .event-line:gt(3)').addClass('hide-box').removeClass('show-box');



        $('#agenda-accordion .collapse .show-more').click(function () {
            if ($(this).parent('.collapse').find('.event-line:gt(3)').hasClass('hide-box')) {
                $(this).parent('.collapse').find('.event-line:gt(3)').addClass('show-box').removeClass('hide-box');
                $(this).text('hide');
            } else {
                $(this).parent('.collapse').find('.event-line:gt(3)').addClass('hide-box').removeClass('show-box');
                $(this).text('more');
            }

        });

        $('#agenda-accordion').on('shown.bs.collapse', function () {
            $('#agenda-accordion').find('.show .card-body .event-line').addClass('show-box');
            $('#agenda-accordion').find('.show .card-body .event-line:gt(3)').addClass('hide-box').removeClass('show-box');
            $('#agenda-accordion').find('.show .show-more').text('more');

        });


    }


    /*
     ** skills
     */
    $('.modal_mentor').on('shown.bs.modal', function (e) {

        $(this).find('.skills-line').each(function () {
            var rate = $(this).find('.current').data('rate');
            $(this).find('.current').animate({
                width: rate + '%'
            }, 1000);

        });

    }).on('hidden.bs.modal', function (e) {
        $(this).find('.skills-line').each(function () {
            $(this).find('.current').css({
                width: 0
            });

        });
    });


    $('[data-fancybox^="quick-view"]').fancybox({
        idleTime: false,
        baseClass: 'fancybox-custom-layout',
        margin: 0,
        gutter: 0,
        infobar: false,
        touch: {
            vertical: false
        },
        buttons: [
            'close'
        ],
        thumbs: {
            autoStart: true,
            axis: 'x',
            parentEl: '.fancybox-inner'
        },
        btnTpl: {
            arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
        },

        transitionEffect: "slide",
        //animationDuration: 300,
        onInit: function (instance) {
            // Create new wrapping element, it is useful for styling
            // and makes easier to position thumbnails
            instance.$refs.inner.wrap('<div class="fancybox-outer"></div>');

        },
        caption: function (instance, item) {
            return $(this).data('caption');

        }
    });

    /*
     ** Partners slick carusel
     */
    //partners carusel
    $('.partners-carusel').slick({
        dots: false,
        //auto: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        prevArrow: '<button type="button" class="slick-prev slick-arrow">' +
        '<svg id="i-arrow-left" viewBox="0 0 32 32" width="24" height="24" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">\n' +
        '    <path d="M10 6 L2 16 10 26 M2 16 L30 16" />\n' +
        '</svg>' +
        '</button>',
        nextArrow: '<button type="button" class="slick-next slick-arrow">' +
        '<svg id="i-arrow-right" viewBox="0 0 32 32" width="24" height="24" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">\n' +
        '    <path d="M22 6 L30 16 22 26 M30 16 L2 16" />\n' +
        '</svg>' +
        '</button>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    /*
    ** Button scroll up
    */

    $(function () {
        var top_show = 300; // В каком положении полосы прокрутки начинать показ кнопки "Наверх"
        var delay = 2000; // Задержка прокрутки

        $(window).scroll(function () {
            if ($(this).scrollTop() > top_show) $('#scroll-up').fadeIn();
            else $('#scroll-up').fadeOut();
        });

        $('#scroll-up').click(function () {
            $('body, html').animate({
                scrollTop: 0
            }, delay);
        });
    });

    /**************************************Scroll_menu***********************************************************/

        // Cache selectors
    var lastId,
        topMenu = $("#main-menu"),
        topMenuHeight = topMenu.outerHeight() + 15,
        // All list items
        menuItems = topMenu.find('.scroll-link'),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function () {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        }),
        noScrollAction = false;

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function (e) {
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
        noScrollAction = true;


        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, {
            duration: 500,
            complete: function () {
                menuItems
                    .parent().removeClass("active")
                    .end().filter("[href='" + href + "']").parent().addClass("active");
                setTimeout(function () {
                    noScrollAction = false;
                }, 10);
            }
        });
        e.preventDefault();
    });

    // Bind to scroll

    $(window).scroll(function () {
        if (!noScrollAction) {
            // Get container scroll position
            var fromTop = $(this).scrollTop() + topMenuHeight;

            // Get id of current scroll item
            var cur = scrollItems.map(function () {
                if ($(this).offset().top < fromTop)
                    return this;
            });
            // Get the id of the current element
            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";

            if (lastId !== id) {
                lastId = id;
                // Set/remove active class
                menuItems
                    .parent().removeClass("active")
                    .end().filter("[href='#" + id + "']").parent().addClass("active");
            }
        }
    });
    /**************************************END Scroll_menu***********************************************************/

});

$('.was-item').click(function() {
    $('.fancybox-thumbs ul li:first').remove();
    $('.fancybox-thumbs ul li:first').addClass('fancybox-thumbs-active')
});

$('.st-custom-button').click(function(e) {
    e.preventDefault();
});
