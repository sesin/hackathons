var forms = ['activists', 'professionals'];
var tabs;
var currentForm;
var hiddenForm;
var currentTab;
var shownElement;


$('a[data-form]').click(function(){
    currentForm = $(this).data('form');
    showForm(currentForm);
});

$("[type=radio], [type=checkbox]").change(function(){
    if ($(this).attr('type') == 'checkbox') {
        if ($(this).is(":checked")) {
            if ($(this).data('checkbox-max') && $(document).find("[name='" + $(this).attr('name') + "']:checked").length > $(this).data('checkbox-max')) {
                this.checked = false;
            } else {
                $($(this).data('show')).show();
            }    
        } else {
            $($(this).data('show')).hide();
        }
    } else if ($(this).attr('type') == 'radio') {
        if($(this).is(":checked") && $(this).data("show")) {
            $($(this).data('show')).show();
            $($(this).data('hide')).hide();
        } else {
            var sib= $(this).parents('.form-group').find('[data-show]');
            $(sib.data("show")).hide();
        }
    }
});

$("#next").click(function(){
    nextPrev(+1);
});

$("#prev").click(function(){
    nextPrev(-1);
});

$("#submit").click(function(){
    if (!validateForm()) {
        return false;
    } else {
        $("#" + currentForm + "-form").submit();
    }
});

function showForm(form) {
    $.each(forms, function(index, value){
        $('#' + value).hide();
    });

    $('#' + form).show();
    tabs = $('#' + form + '-form').find('.tab');
    currentTab = 0;
    showTab(currentTab);
}

function showTab(currentTab) {
    //console.log(currentTab);
    if (currentTab  == 0) {
        $("#registration-forms").hide();
        tabs.hide();
        $(tabs[currentTab]).show();
        $("#next").show();
        $("#prev").show();
    } else if (currentTab == -1) {
        $('#prev').hide();
        $('#next').hide();
        $('#registration-forms').show();
    } else if (currentTab >= tabs.length - 1) {
        $('#next').hide();
        $('#submit').show();
        // console.log(currentForm);
        $(tabs[currentTab]).show();
    } else {
        $("#next").show();
        $('#submit').hide();
        $(tabs[currentTab]).show();
    }

    if (currentTab != -1) {
        temp = Math.floor((currentTab + 1) / tabs.length * 100);
        $(".progress-bar").css("width", temp + "%");
    } else {
        $(".progress-bar").css("width", 0 + "%");
    }
}

function nextPrev(n) {
    if (n == 1 && !validateForm()) return false;
    tabs.hide();
    currentTab = currentTab + n;
    showTab(currentTab);
    $('body,html').animate({scrollTop:0},200);
}


function validateForm() {
    var  valid = true;
    $(document).find(".is-invalid").removeClass('is-invalid');
    x = $(document).find(".tab:visible");
    y = $(x[0]).find(".form-control:visible:not(.optional)");
    z = $(x[0]).find("[type='radio']:visible, [type='checkbox']:visible");

    for (i = 0; i < y.length; i++) {
        if (y[i].value == "") {
            y[i].className += " is-invalid";
            valid = false;
        }
    }

    for (i = 0; i < z.length; i++) {
        if ($(x[0]).find("[name='" + z[i].name + "']:checked").length == 0) {
            z[i].className += " is-invalid";
            valid = false;
        }
    }

    if (!valid){
        $('html, body').animate({scrollTop: ($('.is-invalid').first().offset().top)},200);
    }

    return valid;
}

// Убираем класс ошибки 
$(document).on('input', '.form-control', function () {
    $(this).removeClass('is-invalid');
});

$("[type='radio'], [type='checkbox']").change(function () {
    $(document).find("[name='" + $(this).attr("name") + "']").removeClass('is-invalid');
});