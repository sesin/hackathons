<?php

return [
    'meta-title' => 'Хакатон гражданского общества стран Восточного партнерства',
    'about-the-hackathon' => 'О хакатоне',
    'why-to-participate' => 'Что ты получишь?',
    'mentors' => 'Менторы',
    'agenda' => 'программа',
    'how-it-was' => 'Как это было?',
    'rules-and-faq' => 'Правила и вопросы',
    'registration' => 'регистрация!',
    'news-and-updates' => 'последние новости',
    'read-more' => 'Подробнее',
    'download-guidelines' => 'Скачать руководство для участников',
    'partners' => 'Партнеры',
    'footer-eu' => '<p>Информация, представленная на сайте, защищена авторскими правами и <a href="http://eapcivilsociety.eu/ru/disclaimer_ru" target="_blank">защитой персональных данных</a>. © Европейский Союз, 2017</p>',
    'footer-gdsi' => 'Проект реализуется консорциумом во главе с GDSI Limited',
    'hackathon-starts-in' => 'ДО ХАКАТОНА ОСТАЛОСЬ:',
    'day' => 'день',
    'lectors' => 'Лекторы',
    'contacts' => 'Контакты',
    'more-news' => 'Еще новости',
    'more' => 'Еще',
];
