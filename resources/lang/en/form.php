<?php

return [
	'eap-civil-society-hackathon' => 'EaP Civil Society Hackathon',
	'register' => 'Register',
	'armenia' => 'Armenia',
	'azerbaijan' => 'Azerbaijan',
	'belarus' => 'Belarus',
	'georgia' => 'Georgia',
	'moldova' => 'Moldova',
	'ukraine' => 'Ukraine',
	'poor' => 'Poor',
	'satisfactory' => 'Satisfactory',
	'good' => 'Good',
	'very-good-native' => 'Very good / Native',
	'select-registration-form' => 'Select the registration form',
	'application-for-activists' => 'Application Form for Civil Activists',
	'application-for-professionals' => 'Application form for Professionals and Graphic designers',
	'title' => 'Title',
	'mr' => 'Mr',
	'mrs' => 'Mrs',
	'personal-information' => 'Personal Information',
	'first-name' => 'First name',
	'last-name' => 'Last name',
	'contact-phone' => 'Contact phone No',
	'email' => 'E-mail',
	'residential-address' => 'Residential Address',
	'street-house-apartment' => 'Street, house, apartment',
	'city-town' => 'City/town',
	'country' => 'Country',
	'please-choose' => 'Please, choose',
	'please-specify' => 'Please, specify',
	'citizenship' => 'Country of citizenship',
	'gender' => 'Gender',
	'male' => 'Male',
	'female' => 'Female',
	'language-skills' => 'Language skills',
	'language-competence' => 'Укажите свои навыки владения языками',
	'english' => 'English',
	'russian' => 'Russian',
	'reading' => 'Reading',
	'speaking' => 'Speaking',
	'listening-comprehension' => 'Listening comprehension',
	'ict-tool-description' => 'Описание предлагаемого IT-инструмента (не более 1500 знаков на каждый ответ)',
	'name-of-ict-tool' => 'Title / name of the proposed ICT tool',
	'target-of-ict-tool' => 'Targeted town(s)/province(s) and country(ies)',
	'describe-ict-tool' => 'Describe your ICT tool in brief',
	'problem-to-solve' => 'The problem to be addressed and how your ICT tool will help to solve it',
	'describe-social-problem' => 'Describe here the social problem or development challenge in your home country the proposed ICT tool is addressing. How well do you know the challenge / problem? Who are the target community of the proposed ICT tool: users and beneficiaries? What will happen to them after/if the proposed ICT tool is developed and launched?',
	'technical-implementation-description' => 'Describe the technical implementation of your ICT tool',
	'main-functionale-description' => 'Describe here the main functionale of your ICT tool, and how users will interact with/utilise it',
	'ict-tool-maintanance' => 'Describe how your ICT tool be maintained and updated in future',
	'ict-tool-maintanance-placeholder' => 'If you envisage to cooperate with / engage other actors into the development and maintenance of your ICT tool, including any state actors, please describe it here, too',
	'other-ict-tools' => 'Are there any other ICT tools that tackle the same issue in your home country, other EaP countries, in Europe or in the world?',
	'other-ict-tools-placeholder' => 'If yes, please provide a brief description, a link to the solution and how your proposal differs from existing ICT tools',
	'areas-of-e-development' => 'In your opinion, what areas of e-development of civil society and citizens does your ICT tool contribute to? Please select not more than three areas',
	'e-participation' => 'e-Participation of citizens',
	'e-transparancy' => 'e-Transparency of public sector',
	'e-government' => 'e-Government and e-Services',
	'citizens-mobilization' => 'Citizens mobilization',
	'citizens-to-citizens' => 'Citizens-to-citizens services',
	'cso-transparency' => 'CSO transparency and accountability',
	'other' => 'Other',
	'relevant-experience' => 'Соответствующий опыт: (не более 1000 знаков на каждый ответ)',
	'experience-of-working-or-volunteering' => 'Do you have experience of working or volunteering in civil society organisation, community or civil movement, in any form?',
	'no' => 'No',
	'yes'=> 'Yes',
	'yes-0-2-years' => 'Yes, for 0 - 2 years',
	'yes-2-5-years' => 'Yes, for 2 - 5 years',
	'yes-6-8-years' => 'Yes, for 6 - 8 years',
	'yes-more-8-years' => 'Yes, for over 8 years',
	'currently-working-or-volunteering' => 'Are you currently working or volunteering in CSO, community or civil movement?',
	'please-tick-as-applicable' => 'Выберите наиболее подходящий вариант',
	'not-engaged-in-any-civil-activity' => 'No, I am not engaged in any civil activity now',
	'yes-full-time-employed' => 'Yes, I am full time employed with a civil society organisation',
	'yes-part-time-employed' => 'Yes, I am part time employed with a civil society organisation',
	'yes-i-am-volunteer' => 'Yes, I am a volunteer with a civil society organisation, community or civil movement',
	'no-full-time-education' => 'No, I am in full-time education, please provide details',
	'name-and-links-of-organisations' => 'Provide the name and links (website or social media pages) of the organisation(s) you are associated with (if any)',
	'position-and-how-long' => 'Describe your position and how long you have been working with the organisation(s)',
	'founder-of-organisation' => 'Are you a Founder of the organisation?',
	'organisation-awareness' => 'Is the Board or management of your organisation/community aware of your interest in this Hackathon programme?',
	'letter-of-support' => 'If you have a letter of support from them, please upload it here',
	'ever-been-participated' => 'Have you ever been participated in or lead a project on design and development of any IT products? If yes, can you describe the project and results achieved?',
	'do-you-have-hack-team' => 'Do you have a hack team to work with you at 2018 EaP Civil Society Hackathon?',
	'hack-team-info' => 'Please provide information on the hack team members (not more than two person)',
	'hack-team-member' => 'Hack Team Member',
	'first-and-last-name' => 'First and Last Name',
	'technical-expertise' => 'Technical expertise',
	'role-in-hack-team' => 'Role in the hack team',
	'optional' => 'optional',
	'select-technical-experts' => 'Please select not more than three technical experts you will need for the implementation of your ICT tool proposal',
	'data-collection-experts' => 'Data collection and scraping experts',
	'data-verification-experts' => 'Data verification experts',
	'interactive-map-developers' => 'Interactive map developers',
	'experts-on-gamification' => 'Experts on gamification',
	'data-crowd-sourcing-experts' => 'Data crowd-sourcing experts',
	'mobile-app-developers' => 'Mobile apps developers',
	'web-developers' => 'Web-developers',
	'web-designers' => 'Web-designers',
	'illustrators' => 'Illustrators',
	'data-visualising-graphic-designers' => 'Graphic designers for data visualising',
	'further-personal-information' => 'Any further personal information pertinent to this Hackathon',
	'application-sign' => 'By clicking here you will sign the applicant declaration',
	'submit' => 'Submit',
	'next' => 'Next',
	'prev' => 'Prev',
	'expertise-main-area' => 'Select your main area of expertise',
	'graphic-design' => 'Graphic design',
	'both' => 'Both',
	'employment-status' => 'Employment Status',
	'full-time-employment' => 'Full time employment',
	'part-time-employment' => 'Part time employment',
	'freelancer' => 'Freelancer',
	'in-full-time-education' => 'In full-time education, please provide details',
	'working-in-it-industry-term' => 'How many years (cumulative) have you been working in IT industry or as a designer?',
	'0-2-years' => '0 - 2 years',
	'2-5-years' => '2 - 5 years',
	'6-8-years' => '6 - 8 years',
	'over-8-years' => 'over 8 years',
	'areas-of-expertise' => 'Describe your areas of expertise and professional experience in IT and/or graphic design. Please provide links to successful projects, if available',
	'areas-of-expertise-placeholder' => 'Area of technical expertise, link(s) to project(s)',
	'website-portfolio' => 'Please provide a link(s) to your website or portfolio',
	'experience-of-working-with-civil-society-organisations' => 'Do you have previous experience of working with civil society organisations, local communities or groups of activists?',
	'details-of-projects-and-results' => 'Please provide details of the project(s) and main results',
	'participate-reason' => 'Explain in brief why do you want to participate in 2018 EaP Civil Society Hackathon?',

]; 