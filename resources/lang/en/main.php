<?php

return [
	'meta-title' => 'EaP Civil Society Hackathon',
	'about-the-hackathon' => 'about the hackathon',
	'why-to-participate' => 'why to participate',
	'mentors' => 'mentors',
	'agenda' => 'agenda',
	'how-it-was' => 'how it was in 2017',
	'rules-and-faq' => 'rules and faq',
	'registration' => 'register now!',
	'news-and-updates' => 'news and updates',
	'read-more' => 'Read More',
	'download-guidelines' => 'Download the Guidelines for Applicants',
	'partners' => 'partners',
	'footer-eu' => '<p>The information on this site is subject to a <a href="http://eapcivilsociety.eu/ru/disclaimer_ru" target="_blank">Disclaimer</a>. Copyright Notice and Protection of personal data. © European Union, 2017</p>',
	'footer-gdsi' => 'The project is implemented by the consortium led by GDSI Limited',
	'hackathon-starts-in' => 'THE HACKATHON STARTS IN:',
	'lectors' => 'Lectors',
	'day' => 'day',
	'more-news' => 'More news',
    'contacts' => 'Contacts',
    'more' => 'More',
];
