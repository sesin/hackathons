<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsTranslation extends Model
{
    protected $fillable = ['translated_contacts'];

    protected $casts = [
        'translated_contacts' => 'object',
    ];

    public $timestamps = false;
}
