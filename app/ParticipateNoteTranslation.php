<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipateNoteTranslation extends Model
{
	public $timestamps = false;

    protected $fillable = ['title', 'content'];
}
