<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingsTranslation extends Model
{
    protected $fillable = ['translated_settings'];

    protected $casts = [
        'translated_settings' => 'object',
    ];

    public $timestamps = false;
}
