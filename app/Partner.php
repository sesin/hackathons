<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
	use Translatable;

    protected $fillable = ['link', 'logo'];

    public $translatedAttributes = ['caption'];
}
