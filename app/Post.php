<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{
    use Translatable;

    protected $dates = ['created_at', 'updated_at'];

    public $translatedAttributes = ['title', 'content'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('default_ordering', function (Builder $builder) {
            $builder->orderby('posts.created_at', 'desc');
        });
    }
    
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }
}
