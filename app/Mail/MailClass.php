<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailClass extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (isset($this->data['form']) && $this->data['form']=='activists'){
            // if (isset($this->data['url'])){
            //     return $this->view('mails.activists', $this->data['formData'])->subject('Activists Application Form')->attach($this->data['url']);
            // } else {
                return $this->view('mails.activists', $this->data['formData'])->subject('Activists Application Form');
            // }
        } else {
            return $this->view('mails.professionals', $this->data['formData'])->subject('IT professionals Application Form');
        }
    }
}
