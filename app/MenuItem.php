<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
	use Translatable;
	
	protected $fillabe = ['position_id'];

    public $translatedAttributes = ['text'];


}
