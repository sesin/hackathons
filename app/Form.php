<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = ['type', 'form_data', 'support_letter_url'];

    protected $casts = [
        'form_data' => 'object',
    ];
}
