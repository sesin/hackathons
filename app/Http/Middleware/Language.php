<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $fallbackLocale = session('lang', config('app.fallback_locale'));

        // dd($fallbackLocale);

        if(!$request->is('dashboard*')) {
            $locale = $request->segment(1);

            if (!array_key_exists($locale, config('translatable.locales'))) {
                $segments = array_prepend($request->segments(), $fallbackLocale);

                return redirect(implode(DIRECTORY_SEPARATOR, $segments));
            }

            $fallbackLocale = $locale;
            session(['lang' => $locale]);
        }

        app()->setLocale($fallbackLocale);
        
        if ($fallbackLocale == 'ru') {
            setLocale(LC_ALL, 'ru_RU.utf8');
        }

        return $next($request);
    }
}
