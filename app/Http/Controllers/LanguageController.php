<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class LanguageController extends Controller
{

    /**
     * @param Request $request
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function switchLang(Request $request, $lang)
    {
        /** @var Request $previousRequest */
        $previousRequest = app('request')->create(
            url()->previous()
        );

        if(array_key_exists($lang, config('translatable.locales'))) {
            session(['lang' => $lang]);

            $segments = $previousRequest->segments();
            $locale = array_shift($segments);

            if (array_key_exists($locale, config('translatable.locales'))) {
                return redirect(
                    implode(DIRECTORY_SEPARATOR, array_prepend($segments, $lang))
                );
            }
        }

        return redirect()->back();
    }
}
