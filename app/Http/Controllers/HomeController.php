<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Day;
Use App\Post;
use App\Album;
use App\Mentor;
use App\Partner;
use App\Lecture;
use App\Contacts;
use App\Settings;
use App\ParticipateNote;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Carbon::getLocale());
        // setlocale(LC_TIME, 'Russian');
    	$settings = Settings::find(1);
        $post_news = Post::type('news')->get();
        $participate_notes = ParticipateNote::all();
        $mentors = Mentor::all();
        $rules = Post::type('rule')->get();
        $days = Day::all();
        $partners = Partner::all();
        $contacts = Contacts::find(1);
        $albums = Album::all();

        return view('home', compact('settings', 'post_news', 'participate_notes', 'mentors', 'days', 'rules', 'partners', 'contacts', 'albums'));
    }
}
