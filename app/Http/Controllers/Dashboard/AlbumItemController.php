<?php

namespace App\Http\Controllers\Dashboard;

use Image;
use App\AlbumItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class AlbumItemController extends Controller
{
    public function index()
    {
        $album_items = AlbumItem::all();

        return view('dashboard.album-items.index', compact('album_items'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'url' => 'max:191',
        ]);

        $album_item = new AlbumItem;

        $album_item->type = 'url';
        $album_item->url = $request->url;

        $album_item->save();

        return redirect(route('album-items.index'))
            ->with('flash_message', 'Элемент успешно добавлен!');
    }

    public function uploadImages(Request $request)
    {
    	$this->validate($request, [
    		'image' => 'image|max:2000',
    	]);

    	if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $image = $request->file('image');
            $location = public_path('storage/gallery/');
            $extension = "." . $image->getClientOriginalExtension();
            $image_name = time();
            $original_image_name = $image_name . $extension;
            Storage::putFileAs('public/gallery', $image, $original_image_name); //saving original image
            $original_image_path = 'storage/gallery' . $original_image_name;

            $resolutions = ['large' => '1024', /*'medium' => '512',*/ 'small' => '128'];

            //creating resized images

            foreach ($resolutions as $size => $resolution) {
                Image::make($image)->resize($resolution, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($location . '/' . $image_name . '-' . $resolution . $extension);

                ${$size . "_image_path"} = '/storage/gallery/' . $image_name . '-' . $resolution . $extension;
            }

        } else {
            return redirect('403');
        }
        
        $album_item = new AlbumItem;
        $album_item->type = 'image';
        $album_item->original_image = $original_image_path;
        $album_item->large_image = $large_image_path;
        // $album_item->medium_image = $medium_image_path;
        $album_item->small_image = $small_image_path;
        
        $album_item->save();

        return redirect(route('album-items.index'))
            ->with('flash_message', 'Элемент успешно добавлен!');
    }

    public function destroy(Request $request, $id)
    {
        $album_item = AlbumItem::find($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $album_item->delete();

            return redirect(route('album-items.index'))
                ->with('error_message', 
                    'Элмент  удален');
        } else {
            return view('dashboard.album-items.confirm-delete', compact('album_item'));
        }
    }
}
