<?php

namespace App\Http\Controllers\Dashboard;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::all();

        return view('dashboard.partners.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'caption_ru' => 'required',
            'caption_en' => 'required',
            'link' => 'required',
            'logo' => 'required'
        ]);

        $partner = new Partner;

        $partner->translateOrNew('en')->caption = $request->caption_en;
        $partner->translateOrNew('ru')->caption = $request->caption_ru;

        $partner->link = $request->link;
        $partner->logo = $request->logo;

        $partner->save();

        return redirect()->route('partners.index')
            ->with('flash_message',
                'Партнер добавлен!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partner::findOrFail($id);

        return view('dashboard.partners.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'link' => 'required',
            'logo' => 'required'
        ]);

        $partner = Partner::findOrFail($id);

        $partner->translateOrNew('en')->caption = $request->caption_en;
        $partner->translateOrNew('ru')->caption = $request->caption_ru;

        $partner->link = $request->link;
        $partner->logo = $request->logo;

        $parnter->update();

        return redirect()->route('partners.index')
            ->with('flash_message',
                'Партнер обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $partner = Partner::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $partner->delete();

            return redirect(route('partners.index'))
                ->with('error_message', 
                    'Партнер ' . $partner->name . ' удален');
        } else {
            return view('dashboard.partners.confirm-delete', compact('partner'));
        }
    }
}
