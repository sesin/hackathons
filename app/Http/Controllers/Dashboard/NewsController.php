<?php

namespace App\Http\Controllers\Dashboard;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::type('news')->get();

        return view('dashboard.news.index', compact('posts'));
    }

    public function create()
    {
        return view('dashboard.news.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title_ru' => 'required',
            'title_en' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
        ]);

        $post = new Post;

        $post->type = $request->type;

        $post->translateOrNew('en')->title   = $request->title_en;
        $post->translateOrNew('en')->content = $request->content_en;

        $post->translateOrNew('ru')->title   = $request->title_ru;
        $post->translateOrNew('ru')->content   = $request->content_ru;

        $post->save();

        return redirect()->route('news.index')
            ->with('flash_message',
                'Новость ' . $post->title . ' добавлена!');
    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('dashboard.news.edit', compact('post'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title_ru' => 'required',
            'title_en' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
        ]);

        $post = Post::findOrFail($id);
        
        $post->type = $request->type;
        
        $post->translate('en')->title   = $request->title_en;
        $post->translate('en')->content = $request->content_en;

        $post->translate('ru')->title   = $request->title_ru;
        $post->translate('ru')->content = $request->content_ru;

        $post->update();

        return redirect(route('news.index'))
            ->with('flash_message',
                'Новость ' . $post->title . ' обновлена!');
    }

    public function destroy(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $post->delete();

            return redirect(route('news.index'))
                ->with('error_message', 
                    'Новость ' . $post->title . ' удалена');
        } else {
            return view('dashboard.news.confirm-delete', compact('post'));
        }
    }
}
