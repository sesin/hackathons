<?php

namespace App\Http\Controllers\Dashboard;

use App\Contacts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
	public function index()
    {
        $contacts_model = Contacts::find(1) ? Contacts::find(1) : new Contacts;

        return view('dashboard.contacts.index', compact('contacts_model'));
    }

    public function update(Request $request)
    {
    	// dd($request->contacts);
        $contacts_model = Contacts::find(1);
        $contacts_model->contacts = $request->contacts;

        $contacts_model->translateOrNew('ru')->translated_contacts = $request->translated_contacts_ru;
        // dd($request->translated_contacts_ru);
        $contacts_model->translateOrNew('en')->translated_contacts = $request->translated_contacts_en;

        $contacts_model->update();

        return redirect(route('contacts.index'))
            ->with('flash_message',
                'Контакты обновлены!');
    }
}
