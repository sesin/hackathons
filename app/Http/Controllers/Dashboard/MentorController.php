<?php

namespace App\Http\Controllers\Dashboard;

use App\Mentor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MentorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mentors = Mentor::all();

        return view('dashboard.mentors.index', compact('mentors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.mentors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image_path' => 'required',
            'name_en'    => 'required',
            'name_ru'    => 'required',
        ]);


        $mentor = new Mentor;

        $mentor->image_path = $request->image_path;
        $mentor->twitter_url  = $request->twitter_url;
        $mentor->facebook_url = $request->facebook_url;
        $mentor->linkedin_url = $request->linkedin_url;
        $mentor->skill_one_percentage   = $request->skill_one_percentage;
        $mentor->skill_two_percentage   = $request->skill_two_percentage;
        $mentor->skill_three_percentage = $request->skill_three_percentage;

        $mentor->translateOrNew('en')->name        = $request->name_en;
        $mentor->translateOrNew('en')->profession  = $request->profession_en;
        $mentor->translateOrNew('en')->description = $request->description_en;
        $mentor->translateOrNew('en')->skill_one   = $request->skill_one_en;
        $mentor->translateOrNew('en')->skill_two   = $request->skill_two_en;
        $mentor->translateOrNew('en')->skill_three = $request->skill_three_en;

        $mentor->translateOrNew('ru')->name        = $request->name_ru;
        $mentor->translateOrNew('ru')->profession  = $request->profession_ru;
        $mentor->translateOrNew('ru')->description = $request->description_ru;
        $mentor->translateOrNew('ru')->skill_one   = $request->skill_one_ru;
        $mentor->translateOrNew('ru')->skill_two   = $request->skill_two_ru;
        $mentor->translateOrNew('ru')->skill_three = $request->skill_three_ru;

        $mentor->save();
        
        return redirect()->route('mentors.index')
            ->with('flash_message',
                'Наставник ' . $mentor->name . ' добавлен!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mentor = Mentor::find($id);

        return view('dashboard.mentors.edit', compact('mentor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image_path' => 'required',
            'name_en'    => 'required',
            'name_ru'    => 'required',
        ]);


        $mentor = Mentor::find($id);

        $mentor->image_path   = $request->image_path;
        $mentor->twitter_url  = $request->twitter_url;
        $mentor->facebook_url = $request->facebook_url;
        $mentor->linkedin_url = $request->linkedin_url;
        $mentor->skill_one_percentage   = $request->skill_one_percentage;
        $mentor->skill_two_percentage   = $request->skill_two_percentage;
        $mentor->skill_three_percentage = $request->skill_three_percentage;

        $mentor->translate('en')->name        = $request->name_en;
        $mentor->translate('en')->profession  = $request->profession_en;
        $mentor->translate('en')->description = $request->description_en;
        $mentor->translate('en')->skill_one   = $request->skill_one_en;
        $mentor->translate('en')->skill_two   = $request->skill_two_en;
        $mentor->translate('en')->skill_three = $request->skill_three_en;

        $mentor->translate('ru')->name        = $request->name_ru;
        $mentor->translate('ru')->profession  = $request->profession_ru;
        $mentor->translate('ru')->description = $request->description_ru;
        $mentor->translate('ru')->skill_one   = $request->skill_one_ru;
        $mentor->translate('ru')->skill_two   = $request->skill_two_ru;
        $mentor->translate('ru')->skill_three = $request->skill_three_ru;

        $mentor->save();
        
        return redirect()->route('mentors.index')
            ->with('flash_message',
                'Наставник ' . $mentor->name . ' обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $mentor = Mentor::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $mentor->delete();

            return redirect(route('mentors.index'))
                ->with('error_message', 
                    'Наставник ' . $mentor->name . ' удален');
        } else {
            return view('dashboard.mentors.confirm-delete', compact('mentor'));
        }
    }
}
