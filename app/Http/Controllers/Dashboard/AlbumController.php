<?php

namespace App\Http\Controllers\Dashboard;

use App\Album;
use App\AlbumItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlbumController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::all();

        return view('dashboard.albums.index', compact('albums'));
    }

    public function create()
    {
        $album_items = AlbumItem::all();
        // dd($album_items->count());
        if (!$album_items->count()) {
            return redirect(route('album-items.index'))
                ->with('error_message', 'Вам нужно сначала добавить элементы для создания альбома');
        }

        return view('dashboard.albums.create', compact('album_items'));
    }

    public function store(Request $request)
    {
        if (!$request->album_items) {
            return redirect()->back()
                ->with('error_message', 'Вы не можете создавать пустые альбомы');
        }

        $this->validate($request, [
            'name_ru' => 'required',
            'name_en' => 'required',
            'cover_image_path' => 'required',
        ]);

        $album = new Album;
        $album->cover_image_path = $request->cover_image_path;

        $album->translateOrNew('en')->name   = $request->name_en;
        $album->translateOrNew('ru')->name   = $request->name_ru;

        $album->save();

        $album->albumItems()->sync($request->album_items, false);

        return redirect()->route('albums.index')
            ->with('flash_message',
                'Альбом ' . $album->name . ' добавлен!');
    }

    public function edit($id)
    {
        $album_items = AlbumItem::all();

        $album = Album::find($id);

        return view('dashboard.albums.edit', compact('album', 'album_items'));
    }

    public function update(Request $request, $id)
    {
        if (!$request->album_items) {
            return redirect()->back()
                ->with('error_message', 'Вы не можете создавать пустые альбомы');
        }

        $this->validate($request, [
            'name_ru' => 'required',
            'name_en' => 'required',
            'cover_image_path' => 'required',
        ]);

        $album = Album::findOrFail($id);
        $album->cover_image_path = $request->cover_image_path;
        
        $album->translate('en')->name   = $request->name_en;
        $album->translate('ru')->name   = $request->name_ru;

        $album->update();

        $album->albumItems()->sync($request->album_items);

        return redirect(route('albums.index'))
            ->with('flash_message',
                'Альбом ' . $album->name . ' обновлен!');
    }

    public function destroy(Request $request, $id)
    {
        $album = Album::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $album->delete();

            return redirect(route('albums.index'))
                ->with('error_message', 
                    'Альбом ' . $album->name . ' удален');
        } else {
            return view('dashboard.albums.confirm-delete', compact('album'));
        }
    }
}