<?php

namespace App\Http\Controllers\Dashboard;

use App\Day;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DayController extends Controller
{
    public function index()
    {
        $days = Day::all();

        return view('dashboard.days.index', compact('days'));
    }

    public function create()
    {
        return view('dashboard.days.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required|unique:days'
        ]);

        $day = Day::create(
        	$request->only('date')
        );

        return redirect()->route('days.index')
            ->with('flash_message',
                'День ' . $day->date . ' добавлен!');
    }

    public function edit($id)
    {
        $day = Day::find($id);

        return view('dashboard.days.edit', compact('day'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required|unique:days,date,' . $id,
        ]);

        $day = Day::findOrFail($id);

        $day->update(
        	$request->only('date')
        );

        return redirect(route('days.index'))
            ->with('flash_message',
                'День ' . $day->date . ' обновлен!');
    }

    public function destroy(Request $request, $id)
    {
        $day = Day::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $day->delete();

            return redirect(route('days.index'))
                ->with('error_message', 
                    'Новость ' . $day->title . ' удалена');
        } else {
            return view('dashboard.days.confirm-delete', compact('day'));
        }
    }
}
