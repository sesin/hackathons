<?php

namespace App\Http\Controllers\Dashboard;

use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings_model = Settings::find(1);

        return view('dashboard.index', compact('settings_model'));
    }

    public function update(Request $request)
    {
        $settings = Settings::find(1);
        
        $settings->settings = $request->settings;

        $settings->translateOrNew('ru')->translated_settings = $request->translated_settings_ru;
        $settings->translateOrNew('en')->translated_settings = $request->translated_settings_en;

        $settings->update();

        return redirect(route('dashboard'))
            ->with('flash_message',
                'Настройки обновлены!');
    }
}
