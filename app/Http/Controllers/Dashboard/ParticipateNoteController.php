<?php

namespace App\Http\Controllers\Dashboard;

use App\ParticipateNote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParticipateNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participate_notes = ParticipateNote::all();

        return view('dashboard.participate-notes.index', compact('participate_notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.participate-notes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title_ru' => 'required',
            'title_en' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
        ]);

        $participate_note = new ParticipateNote;

        $participate_note->translateOrNew('en')->title   = $request->title_en;
        $participate_note->translateOrNew('en')->content = $request->content_en;
        $participate_note->translateOrNew('ru')->title   = $request->title_ru;
        $participate_note->translateOrNew('ru')->content   = $request->content_ru;

        $participate_note->save();
        
        return redirect()->route('participate-notes.index')
            ->with('flash_message',
                'Запись ' . $participate_note->title . ' добавлена!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participate_note = ParticipateNote::findOrFail($id);

        return view('dashboard.participate-notes.edit', compact('participate_note'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title_ru' => 'required',
            'title_en' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
        ]);

        $participate_note = ParticipateNote::findOrFail($id);

        $participate_note->translateOrNew('en')->title   = $request->title_en;
        $participate_note->translateOrNew('en')->content = $request->content_en;
        $participate_note->translateOrNew('ru')->title   = $request->title_ru;
        $participate_note->translateOrNew('ru')->content   = $request->content_ru;

        $participate_note->update();

        return redirect()->route('participate-notes.index')
            ->with('flash_message',
                'Запись ' . $participate_note->title . ' обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $participate_note = ParticipateNote::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $participate_note->delete();

            return redirect(route('participate-notes.index'))
                ->with('error_message', 
                    'Новость ' . $participate_note->title . ' удалена');
        } else {
            return view('dashboard.participate-notes.confirm-delete', compact('participate_note'));
        }
    }
}
