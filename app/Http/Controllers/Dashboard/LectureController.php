<?php

namespace App\Http\Controllers\Dashboard;

use App\Day;
use App\Mentor;
use App\Lecture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LectureController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lectures = Lecture::all();

        return view('dashboard.lectures.index', compact('lectures'));
    }

    public function create()
    {
        $mentors = Mentor::all();
        $days = Day::all()->pluck('date', 'id')->prepend('--', 0);

        return view('dashboard.lectures.create', compact('mentors', 'days'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'start_at' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',

        ]);

        $lecture = new Lecture;

        $lecture->start_at = $request->start_at;
        $lecture->end_at   = $request->end_at;
        $lecture->day_id   = $request->day_id == 0 ? null : $request->day_id;

        $lecture->translateOrNew('en')->title   = $request->title_en;
        $lecture->translateOrNew('en')->place   = $request->place_en;
        $lecture->translateOrNew('en')->content = $request->content_en;

        $lecture->translateOrNew('ru')->title   = $request->title_ru;
        $lecture->translateOrNew('ru')->place   = $request->place_ru;
        $lecture->translateOrNew('ru')->content = $request->content_ru;

        $lecture->save();

        $lecture->mentors()->sync($request->mentors, false);

        return redirect()->route('lectures.index')
            ->with('flash_message',
                'Лекция ' . $lecture->title . ' добавлена!');
    }

    public function edit($id)
    {
        $mentors = Mentor::all();
        $days = Day::all()->pluck('date', 'id')->prepend('--', 0);
        $lecture = Lecture::find($id);


        return view('dashboard.lectures.edit', compact('lecture', 'mentors', 'days'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'start_at' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
        ]);

        $lecture = Lecture::findOrFail($id);

        $lecture->start_at = $request->start_at;
        $lecture->end_at   = $request->end_at;

        $lecture->day_id   = $request->day_id == 0 ? null : $request->day_id;

        $lecture->translateOrNew('en')->title   = $request->title_en;
        $lecture->translateOrNew('en')->place   = $request->place_en;
        $lecture->translateOrNew('en')->content = $request->content_en;

        $lecture->translateOrNew('ru')->title   = $request->title_ru;
        $lecture->translateOrNew('ru')->place   = $request->place_ru;
        $lecture->translateOrNew('ru')->content = $request->content_ru;

        $lecture->update();

        $lecture->mentors()->sync($request->mentors);

        return redirect(route('lectures.index'))
            ->with('flash_message',
                'Лекция ' . $lecture->title . ' обновлена!');
    }

    public function destroy(Request $request, $id)
    {
        $lecture = Lecture::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $lecture->delete();

            return redirect(route('lectures.index'))
                ->with('error_message', 
                    'Новость ' . $lecture->title . ' удалена');
        } else {
            return view('dashboard.lectures.confirm-delete', compact('lecture'));
        }
    }
}
