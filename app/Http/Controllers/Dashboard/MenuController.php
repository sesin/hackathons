<?php

namespace App\Http\Controllers\Dashboard;

use App\MenuItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function index()
    {
        $menu_items = MenuItem::all();

        return view('dashboard.menu.index', compact('menu_items'));
    }

    public function create()
    {
        return view('dashboard.menu.create');
    }

    public function store(Request $request)
    {
        $menu_item = new MenuItem;

        $menu_item->translateOrNew('en')->text   = $request->text_en;

        $menu_item->translateOrNew('ru')->text   = $request->text_ru;

        $menu_item->save();

        return redirect()->route('menu.index')
            ->with('flash_message',
                'Пункт меню ' . $menu_item->text . ' добавлен!');
    }

    public function edit($id)
    {
        $menu_item = MenuItem::find($id);

        return view('dashboard.menu.edit', compact('menu_item'));
    }

    public function update(Request $request, $id)
    {
        $menu_item = MenuItem::findOrFail($id);

        $menu_item->translateOrNew('en')->text   = $request->text_en;

        $menu_item->translateOrNew('ru')->text   = $request->text_ru;

        return redirect(route('menu.index'))
            ->with('flash_message',
                'Пункт меню ' . $menu_item->text . ' обновлен!');
    }

    public function destroy(Request $request, $id)
    {
        $menu_item = MenuItem::findOrFail($id);

        if($request->has('confirmed') && $request->get('confirmed')) {
            $menu_item->delete();

            return redirect(route('menu.index'))
                ->with('error_message', 
                    'Пункт меню ' . $menu_item->text . ' удалена');
        } else {
            return view('dashboard.menu.confirm-delete', compact('menu_item'));
        }
    }

    public function positionsUpdate(Request $request)
    {
        //
    }
}
