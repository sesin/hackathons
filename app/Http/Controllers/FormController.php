<?php

namespace App\Http\Controllers;

use App\Form;
use App\Settings;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Mail\MailClass;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function index()
    {
        $countries = [
            'Armenia' => trans('form.armenia'),
            'Azerbaijan' => trans('form.azerbaijan'),
            'Belarus' => trans('form.belarus'),
            'Georgia' => trans('form.georgia'),
            'Moldova' => trans('form.moldova'),
            'Ukraine' => trans('form.ukraine')
        ];

        $language_levels = [
            'Poor' => trans('form.poor'),
            'Satisfactory' => trans('form.satisfactory'),
            'Good' => trans('form.good'),
            'Very good' => trans('form.very-good-native'),
        ];

        $experience_of_working = [
            'No' => trans('form.no'),
            'Yes, for 0 - 2 years' => trans('form.yes-0-2-years'),
            'Yes, for 2 - 5 years' => trans('form.yes-2-5-years'),
            'Yes, for 6 - 8 years' => trans('form.yes-6-8-years'),
            'Yes, for over 8 years' => trans('form.yes-more-8-years'),
        ];
        
        return view('forms.index', compact('countries', 'language_levels', 'experience_of_working'));
    }

    public function favorites()
    {
        $countries = [
            'Armenia' => trans('form.armenia'),
            'Azerbaijan' => trans('form.azerbaijan'),
            'Belarus' => trans('form.belarus'),
            'Georgia' => trans('form.georgia'),
            'Moldova' => trans('form.moldova'),
            'Ukraine' => trans('form.ukraine')
        ];

        $language_levels = [
            'Poor' => trans('form.poor'),
            'Satisfactory' => trans('form.satisfactory'),
            'Good' => trans('form.good'),
            'Very good' => trans('form.very-good-native'),
        ];

        $experience_of_working = [
            'No' => trans('form.no'),
            'Yes, for 0 - 2 years' => trans('form.yes-0-2-years'),
            'Yes, for 2 - 5 years' => trans('form.yes-2-5-years'),
            'Yes, for 6 - 8 years' => trans('form.yes-6-8-years'),
            'Yes, for over 8 years' => trans('form.yes-more-8-years'),
        ];
        
        return view('forms.favorites', compact('countries', 'language_levels', 'experience_of_working'));
    }

    public function storeProfessionals(Request $request)
    {

        $data = [
                'formData' => $request->formData,
                'form' => 'professionals',
            ];
        Mail::to('hackathons@eapcivilsociety.eu')->cc('shakhdon@gmail.com')->send(new MailClass($data));

        $form = new Form;
        $form->type = $request->type;
        $form->form_data = $request->formData;
        
        $form->save();

        return view('forms.sended');

        // return redirect(route('home'))
        //     ->with('flash_message', __('main.welcome'));
    }


    public function storeActivists(Request $request)
    {
        
        if ($request->hasFile('supportLetter') && $request->file('supportLetter')->isValid()) {
            $path = $request->file('supportLetter')->store('public/files/from forms');
            $path = str_replace('public', "", $path);
            $url = asset('storage' . DIRECTORY_SEPARATOR . $path);
        }

        $data = [
                'formData' => $request->formData,
                'form' => 'activists',
            ];
        if (isset($url)) { $data['url'] = $url; }

        Mail::to('hackathons@eapcivilsociety.eu')->cc('shakhdon@gmail.com')->send(new MailClass($data));
                
        $form = new Form;
        $form->type = $request->type;
        $form->form_data = $request->formData;
        if (isset($url)) $form->support_letter_url = $url;
        
        $form->save();

        return view('forms.sended');

        // return redirect(route('home'))
        //     ->with('flash_message', 'Thank you!');
    }
}
