<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumTranslation extends Model
{

    protected $fillable = ['name'];

    public $timestamps = false;

}