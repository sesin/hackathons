<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItemTranslation extends Model
{
    protected $fillable = ['text'];

    public $timestamps = false;
}
