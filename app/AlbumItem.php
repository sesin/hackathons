<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumItem extends Model
{
    protected $fillable = ['url'];

    public $timestamps = false;

    public function albums()
    {
        return $this->belongsToMany('App\Album');
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }
}
