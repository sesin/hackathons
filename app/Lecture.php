<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    use Translatable;

    public $translatedAttributes = ['place', 'title', 'content'];

    protected $fillable = ['start_at', 'end_at'];
    
    public function mentors()
    {
        return $this->belongsToMany('App\Mentor');
    }

    public function day()
    {
    	return $this->belongsTo('App\Day');
    }
}
