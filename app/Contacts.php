<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    use Translatable;

    protected $fillable = ['contacts'];

    protected $casts = [
        'contacts' => 'object',
    ];

    public $timestamps = false;
    public $translatedAttributes = ['translated_contacts'];
}
