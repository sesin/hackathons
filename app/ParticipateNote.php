<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ParticipateNote extends Model
{
	use Translatable;

	public $translatedAttributes = ['title', 'content'];
}
