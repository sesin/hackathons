<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use Translatable;

    protected $fillable = ['settings'];

    protected $casts = [
        'settings' => 'object',
    ];

    public $timestamps = false;
    public $translatedAttributes = ['translated_settings'];
}
