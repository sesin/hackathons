<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;

class Mentor extends Model
{
    use Translatable;

    public $translatedAttributes = [
        'name', 'profession', 'description', 
        'skill_one', 'skill_two', 'skill_three'
    ];

    protected $fillable = [
        'image_path',
        'twitter_url', 'facebook_url', 'linkedin_url',
        'skill_one_percentage', 'skill_two_percentage', 'skill_three_percentage'
    ];

    public function lectures()
    {
        return $this->belongsToMany('App\Lecture');
    }
}
