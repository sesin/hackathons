<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MentorTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'profession', 'description',
        'skill_one', 'skill_two', 'skill_three'
    ];
}
