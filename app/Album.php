<?php

namespace App;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use Translatable;

    protected $fillable = ['cover_image'];

    public $translatedAttributes = ['name'];

    public function albumItems()
    {
    	return $this->belongsToMany('App\AlbumItem');
    }
}