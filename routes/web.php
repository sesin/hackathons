<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{language}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang'
]);

Route::group(['middleware' => 'language'], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/register-form', ['as' => 'form.register','uses' => 'FormController@index']);
    Route::get('/register-form-for-favorites', ['as' => 'form.register','uses' => 'FormController@favorites']);
    Route::post('/professionals-form-store', ['as' => 'professionals.form.store','uses' => 'FormController@storeProfessionals']);
    Route::post('/activists-form-store', ['as' => 'activists.form.store','uses' => 'FormController@storeActivists']);
    
    Auth::routes();
});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'middleware' => ['auth']], function() {
    App::setlocale('ru');
    Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::post('/update', ['as' => 'dashboard.update-settings', 'uses' => 'DashboardController@update']);
    Route::get('/contacts', ['as' => 'contacts.index', 'uses' => 'ContactsController@index']);
    Route::post('/contacts', ['as' => 'contacts.update', 'uses' => 'ContactsController@update']);
    Route::resource('news', 'NewsController', ['except' => 'show']);
    Route::resource('participate-notes', 'ParticipateNoteController', ['except' => 'show']);
    Route::resource('partners', 'PartnerController', ['except' => 'show']);
    Route::resource('mentors', 'MentorController', ['except' => 'show']);
    Route::resource('days', 'DayController', ['except' => 'show']);
    Route::resource('lectures', 'LectureController', ['except' => 'show']);
    Route::resource('rules', 'RuleController', ['except' => 'show']);
    Route::resource('menu', 'MenuController', ['except' => 'show']);
    Route::resource('albums', 'AlbumController', ['except' => 'show']);
    Route::resource('album-items', 'AlbumItemController', ['except' => 'show']);
    Route::post('upload-images', ['as' => 'album-items.upload-images', 'uses' => 'AlbumItemController@uploadImages']);

    Route::post('/menu/positions-update', ['as' => 'menu.positions-update', 'uses' => 'MenuController@positionsUpdate']);
});
